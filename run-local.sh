#!/bin/bash

SERVICE=$1
TYPE=$2

if [ -z "$SERVICE" ]
  then
    echo "A service name is required"
    exit 1
fi

if [ "$SERVICE" = "vonage" ];
  then
  if [ -z "$TYPE" ]
    then
        echo "Type is required for $SERVICE"
        exit 1
  fi
fi

echo removing contents from the dist folder
rm ./dist/*

echo building and installing app
python setup.py install

echo running app
$SERVICE $TYPE

