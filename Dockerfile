FROM as-docker-registry.cartrack.com/internal/ce-base:3.6.9-alpine-1 as base

FROM as-docker-registry.cartrack.com/internal/ce-builder-base:3.6.9-alpine-1 as builder
WORKDIR /install
COPY ["dist", "dist/"]
RUN pip install ./dist/*.whl

FROM base
WORKDIR /usr/local
RUN mkdir config && mkdir keys
COPY --from=builder /usr/local ./