import psycopg2
import psycopg2.extras
import os
import logging
import time
import select
import sys
import datetime
import functools
import json
from collections import deque

from ct.service import ServiceBase


svc = ServiceBase()

fetch_limit=10
max_connect_attempts = 5
#log_filename = 'ct_sm_reporter.log'

# device_name = 'cartrack_link'
comms_cartrack_medium_map = {'wismo':1, 'ssmi':1, 'smpp':1, 'modem':1, 'modem_pool':1, 'mswitch':3, 'mswitch_rx':3, 'lbs':4, 'integrat':4}
text_mode_units = tuple(['SG', 'SH', 'PT', 'JP', 'GS', 'IN'])
loglevel = logging.DEBUG

logging.basicConfig(level=loglevel,format='%(asctime)s %(levelname)s %(message)s')

SM_SENDING=2
SM_NEW=0
SM_DELIVERED=4
CT_UPDATE_INTERVAL = 3*60 #3 minutes
ONLINE_REPORT_INTERVAL = 60 #1 minute
QUEUE_REPORT_INTERVAL = 2

AUTOSMS_APPLICATION_ID = 1030

comms_dsn = "dbname=%s host=%s user=%s password=%s" % (svc.conf['db_name'], svc.conf['db_host'], svc.conf['db_user'], svc.conf['db_pass'])
ct_dsn = "dbname=%s host=%s user=%s password=%s" % (svc.conf['ct_db_name'], svc.conf['ct_db_host'], svc.conf['ct_db_user'], svc.conf['ct_db_pass'])

local_address = svc.conf.get('host_address', '')

[comms_conn, comms_cur, ct_conn, ct_cur] = [None, None, None, None]

def connect_dsn (dsn, asyncronous=0):
    i = 1
    while i <= max_connect_attempts:
        try:
            conn = psycopg2.connect (dsn, async=asyncronous)
            if asyncronous == 0:
                conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
            #cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            return conn
        except psycopg2.OperationalError as e:
            logging.exception(e)
            i+=1
    sys.exit(1)


def reconnect_comms():
    global comms_conn
    global comms_cur
    if comms_cur is not None:
        comms_cur.close()
    if comms_conn is not None:
        comms_conn.close()
    
    comms_conn = connect_dsn(comms_dsn)
    comms_cur = comms_conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    # comms_cur.execute("LISTEN sm_rx;")
    # comms_cur.execute("LISTEN sm_tx;")
    # comms_cur.execute("LISTEN update_replypath;")
 
def reconnect_ct():
    global ct_conn
    global ct_cur
    if ct_cur is not None:
        ct_cur.close()
    if ct_conn is not None:
        ct_conn.close()
    
    ct_conn = connect_dsn(ct_dsn)
    ct_cur = ct_conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    # ct_cur.execute("LISTEN notify_send_sms;")
    # ct_cur.execute("LISTEN notify_send_autosms;")
    ct_cur.execute("LISTEN sm_to_client;")


class Worker:
    def __init__(self):
        self.comms_conn = connect_dsn(comms_dsn, asyncronous=1)
        self.ct_conn = connect_dsn(ct_dsn, asyncronous=1)
        self.busy = True
        self.callback = self.connected
        self.active_sm_id = -1


    def poll_ok(self):
        if self.callback is None:
            return
        cb = self.callback
        self.callback = None
        cb()


    def connected(self):
        self.comms_cur = self.comms_conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        self.ct_cur = self.ct_conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        self.busy = False


    def reconnect(self):
        self.busy = True
        if self.comms_conn is not None and not self.comms_conn.closed:
            self.comms_conn.close()
        if self.ct_conn is not None and not self.ct_conn.closed:
            self.ct_conn.close()
        self.comms_conn = connect_dsn(comms_dsn, asyncronous=1)
        self.ct_conn = connect_dsn(ct_dsn, asyncronous=1)
        self.callback = self.connected
        
    
    
    def process_rx(self, payload):
        logging.debug ("OUT: new comms -> cartrack sms %s" % (payload))
        self.busy = True
        self.callback = self.process_rx_sm_fetched
        sm_id = int(payload.split(",")[0])
        self.comms_cur.execute ("select sm_rx_unex_id as id, sender, encode(data, 'hex') as data, terminal_serial, ct_processed, target, " \
                                "driver.name as driver_name, lower(driver_type) as driver_type, sent_ts " \
                                "from se.sm_rx_unex, se.driver where sm_rx_unex_id = %s and driver_id=driver.id", [sm_id])


    def process_rx_sm_fetched(self):
        sm = self.comms_cur.fetchone()
        self.active_sm_id = sm['id']
        
        if sm is not None and (sm["ct_processed"] is None or not sm["ct_processed"]):
            if sm['driver_type'] == 'integrat' and sm['target'] and sm['target'][0] == '*':
                logging.info ("Submitting ussd request %s from %s" % (sm['target'], sm['sender']))
                self.callback = self.process_rx_sm_submitted
                self.ct_cur.execute ("select * from tc.sp_process_ussd_call(%s,%s)", [ sm['sender'], sm['target'] ])
            elif sm['driver_name'] == 'ssmi_marketing':
                logging.info ("Submitting marketing cc_autosms reply from %s" % (sm['sender']))
                self.callback = self.process_rx_sm_submitted
                logging.debug ("Storing autosms: %s" % (sm['data']))
                self.ct_cur.execute ("select cc.sp_process_sms(%s,now(),now(),encode(decode(%s, 'hex'),'escape'))", [ sm['sender'], str(sm['data']) ])
            else:
                logging.info ("OUT: Submitting rx sm id: %d, sender: %s, received via %s" % (sm['id'], sm['sender'], sm['driver_name']))
                
                driver_type = 1
                if comms_cartrack_medium_map.has_key(sm['driver_type']):
                    driver_type = comms_cartrack_medium_map[ sm['driver_type'] ]
                else:
                    logging.error ("Don't know how to map driver type %s to the cartrack medium id" % (sm['driver_type']))
                
                self.callback = self.process_rx_sm_submitted
                self.ct_cur.execute ("select * from tc.sp_process_comms_message(%s, %s, now(), coalesce(%s,now()), decode(%s, 'hex'), %s, %s)", \
                                [ sm['sender'], sm['terminal_serial'], sm['sent_ts'], sm['data'], sm['driver_name'], driver_type ])
        else:
            logging.error ("New received sms id %d disappeared or has already been processed" % (sm_id))
            self.busy = False
    
    
    def process_rx_sm_submitted(self):
        for ct_r in self.ct_cur:
            logging.info ("The message was submitted to cartrack with the result for sm id %d: %s" % (self.active_sm_id, ct_r[0])) 
        self.callback = self.process_rx_done
        self.comms_cur.execute ("update se.sm_rx_unex set ct_processed=true where sm_rx_unex_id = %s", [self.active_sm_id])
    
    
    def process_rx_done(self):
        self.active_sm_id = -1
        self.busy = False


    def process_tx(self, payload):
        logging.debug ("sms status change, id: %s" % (payload))
        payload = payload.split(",")
        sm_id = int(payload[1])
        sm_state = int(payload[2])
        if sm_state != SM_NEW:
            self.busy = True
            self.active_sm_state = sm_state
            self.callback = self.process_tx_sm_fetched
            query = "select sm_tx_queue_id as id, sm_state, state_qual, source_sm_id::integer, sender, " \
                    "drv1.name as driver_name_sm, lower(drv1.driver_type) as driver_type_sm, " \
                    "drv2.name as driver_name_attempt, lower(drv2.driver_type) as driver_type_attempt " \
                    "from ((se.sm_tx_queue left join se.delivery_attempt on (sm_tx_queue_id=sm_id and attempt_active=true)) " \
                    "left join se.driver as drv1 on device=drv1.name) left join se.driver as drv2 on delivery_attempt.driver_id=drv2.id " \
                    "where sm_tx_queue_id=%s"
            self.comms_cur.execute (query, [sm_id])
                                                                                                                                                                                                
        else:
            logging.debug ("SM %d is NEW. Skipping" % (sm_id))
    
    
    def process_tx_sm_fetched(self):
        sm = self.comms_cur.fetchone()
        self.comms_cur.fetchall()
        self.active_sm_id = sm['id']
        if sm is not None and sm['sender'] == 'tc_db_interface':
            logging.info ("OUT: Submitting status change for sm id: %d, state: %d, state_qual: %d, last delivery driver: %s" \
                      % (sm['id'], self.active_sm_state, sm['state_qual'], sm['driver_name_sm'] if sm['driver_name_sm'] is not None else sm['driver_name_attempt']))
            
            if (self.active_sm_state == SM_SENDING and (sm['driver_type_attempt'] is not None or sm['driver_type_sm'] is not None)) \
                or (self.active_sm_state != SM_SENDING and sm['driver_type_sm'] is not None):
                
                driver_type = sm['driver_type_sm'] if sm['driver_type_sm'] is not None else sm['driver_type_attempt']
                driver_name = sm['driver_name_sm'] if sm['driver_name_sm'] is not None else sm['driver_name_attempt']
                driver_type_ct = 1
                if comms_cartrack_medium_map.has_key(driver_type):
                    driver_type_ct = comms_cartrack_medium_map[ driver_type ]
                else:
                    logging.error ("Don't know how to map driver type %s to the cartrack medium id" % (driver_type))
                
                self.callback = self.process_tx_submitted
                self.ct_cur.execute ("select * from ct.sp_terminal_request_update (%s,%s,%s,%s,%s)", \
                                [ sm['source_sm_id'], self.active_sm_state, sm['state_qual'], driver_name, driver_type_ct ])
            else:
                logging.error ("SM %d has state %d and no associated sending device" % (sm['id'], self.active_sm_state))
                self.process_tx_done()
                
        elif sm is not None and sm['sender'] == 'ct_autosms' and self.active_sm_state == SM_DELIVERED:
            logging.info ("OUT: Submitting an autosms delivered status for the sm id: %d, autosms id: %d" % (sm['id'], sm['source_sm_id']))
            self.callback = self.process_tx_submitted
            self.ct_cur.execute ("select * from cc.autosms_delivered(%s)", [ sm['source_sm_id'] ])
        
        elif sm is not None:
            logging.info ("Skipping, the sender is %s and the state is %d" % (sm['sender'], self.active_sm_state))
            self.process_tx_done()
        else:
            logging.error ("SMS id %d which just changed status disappeared" % (sm['id']))
            self.process_tx_done()
    
    
    def process_tx_submitted(self):
        for ct_r in self.ct_cur:
            logging.info ("sp_terminal_request_update reply for sm id %d: %s" % (self.active_sm_id, ct_r[0]))
        self.process_tx_done()
    
    
    def process_tx_done(self):
        self.active_sm_id = -1
        self.active_sm_state = -1
        self.busy = False



    def process_cartrack_tx(self, payload):
        self.busy = True
        req_id = int(payload)
        self.active_sm_id = req_id
        self.callback = self.process_cartrack_tx_fetched
        self.ct_cur.execute ("select tc.sp_cfg_unit_msisdn(terminal_id) as target, tc.sp_cfg_unit_terminal_serial(terminal_id) as terminal_serial, "\
                        "final_request_data::bytea, application_id, terminal_id, lower(tm.name) as medium " \
                        "from ct.t_terminal_request tr, ct.terminal_comm_medium tm " \
                        "where tr.terminal_comm_medium_id=tm.terminal_comm_medium_id and terminal_request_id=%s", [req_id])
    
    
    def process_cartrack_tx_fetched(self):
        sm = self.ct_cur.fetchone()
        if sm is not None:
            if sm['final_request_data'] is not None:
                target = 'lbs:' + sm['target'] if sm['medium'] == 'lbs' else sm['target']
                #application_id,target,terminal_serial,text_mode,validity,data,priority_level,sender
                logging.debug ("IN: new cartrack -> comms request terminal_request_id=%d, terminal_id=%d"  % (self.active_sm_id, sm['terminal_id']))
                self.callback = self.process_cartrack_tx_submitted
                #SG and SH units - text messages only
                #PT,JP - also
                text_mode = 1 if sm['terminal_serial'].startswith(text_mode_units) else 0
                self.comms_cur.execute ("insert into se.sm_tx_queue " \
                                   "(application_id,target,terminal_serial,text_mode,validity,data,priority_level,sender,source_sm_id) " \
                                   "values (%s,%s,%s,%s,%s,%s,%s,%s,%s)", [sm['application_id'], target, sm['terminal_serial'], text_mode, 10080, \
                                   sm['final_request_data'], 2, 'tc_db_interface', self.active_sm_id] )
            else:
                logging.error ("New message from the CT db, terminal_request_id: %d has empty data" % (self.active_sm_id))
                self.busy = False
        else:
            logging.error ("New message from the CT db, terminal_request_id: %d suddenly disappeared" % (self.active_sm_id))
            self.busy = False
    
    
    def process_cartrack_tx_submitted(self):
        self.active_sm_id = -1
        self.busy = False
    
    
    def process_cartrack_autosms(self, payload):
        logging.debug ("New cartrack autosms id: %s" % (payload))
        self.busy = True
        req_id = int(payload)
        self.active_sm_id = req_id
        self.callback = self.process_cartrack_autosms_fetched
        self.ct_cur.execute ('select * from cc."cc$auto_sms" where "cc$auto_sms_id"=%s', [req_id])
    
    
    def process_cartrack_autosms_fetched(self):
        sm = self.ct_cur.fetchone()
        msisdn = ''.join(map(lambda x: x if x.isdigit() else '', sm['msisdn']))
        if len(msisdn) > 0 and msisdn[0] == '0':
            msisdn = '27' + msisdn[1:]
        x = datetime.timedelta(0, 60 * sm['queue_interval'] if sm['queue_interval'] > 0 else 60 )
        tm = datetime.datetime.now()
        while tm.hour < 8 or tm.hour > 16 or tm.hour == 12 or tm.hour == 12 or tm.weekday() > 4:
            tm = tm + x
        self.callback = self.process_cartrack_autosms_stored
        self.comms_cur.execute ("insert into se.sm_tx_queue " \
                           "(application_id,target,text_mode,validity,data,sender,source_sm_id,postdate_ts,priority_level) " \
                           "values (%s,%s,%s,%s,%s,%s,%s,%s,%s)", [ AUTOSMS_APPLICATION_ID, msisdn, 1, 10080, sm['payload'], 'ct_autosms', self.active_sm_id, sm['postdate'], sm['priority'] ])
    
    
    def process_cartrack_autosms_stored(self):
        self.callback = self.process_cartrack_autosms_done
        self.ct_cur.execute ('update cc."cc$auto_sms" set processed = 1 where "cc$auto_sms_id" = %s', [self.active_sm_id])
    
    
    def process_cartrack_autosms_done(self):
        self.active_sm_id = -1
        self.busy = False

    def process_cartrack_socialmedia(self, payload):
        logging.debug ("New cartrack social media message: %s" % (payload))
        self.busy = True
        payload_obj = json.loads(payload)
        req_id = int(payload_obj.get('out_message_id', 0))
        self.active_sm_id = req_id
        self.callback = self.process_cartrack_socialmedia_fetched
        self.ct_cur.execute ("""select * from cc."cc$auto_in_socialmedia" sm left 
        join cc.sm_bulk_send_schedule sch on sm.bulk_schedule_id = sch.sm_bulk_send_schedule_id
        where sm."cc$auto_in_socialmedia_id" = %s;""", [req_id])

    def process_cartrack_socialmedia_fetched(self):
        sm = self.ct_cur.fetchone()
        msg_type = sm.get('type_msg', '').lower()
        response = sm.get('response', '').replace("'", "''")
        formatted_response = sm.get('formatted_response', '')
        if formatted_response:
            response = formatted_response

        sch_date = sm.get('scheduled_for', None)
        sch_date_str = 'NULL'
        if sch_date:
            sch_date_str = sch_date.strftime("%Y-%m-%d %H:%M")
            
        msisdn = ''.join(map(lambda x: x if x.isdigit() else '', sm['msisdn']))
        if len(msisdn) > 0 and msisdn[0] == '0':
            msisdn = '27' + msisdn[1:]
        x = datetime.timedelta(0, 60)
        tm = datetime.datetime.now()
        while tm.hour < 8 or tm.hour > 16 or tm.hour == 12 or tm.hour == 12 or tm.weekday() > 4:
            tm = tm + x
        self.callback = self.process_cartrack_socialmedia_stored
        sql = "select * from se.sm_submit('" + msg_type + ":" + msisdn + "', '" + response + "', false, 500, true, false, NULL, 9::smallint, -1);"
        if sch_date:
            sql = "select * from se.sm_submit('" + msg_type + ":" + msisdn + "', '" + response + "', false, 500, true, false, NULL, 9::smallint, -1, '" + sch_date_str + "');"
        logging.debug(sql)
        try:
            self.comms_cur.execute (sql)
        except:
            logging.debug("An error occured while executing the query")

    def process_cartrack_socialmedia_stored(self):
        tx_queue_id = self.comms_cur.fetchone()[0]
        self.callback = self.process_cartrack_socialmedia_done
        self.ct_cur.execute ('update cc."cc$auto_in_socialmedia" set processed = true, sm_tx_queue_id=%s, sm_state=1 where "cc$auto_in_socialmedia_id" = %s', [tx_queue_id, self.active_sm_id])
    
    
    def process_cartrack_socialmedia_done(self):
        self.active_sm_id = -1
        self.busy = False

def main():

    reconnect_comms()
    reconnect_ct()

    tx_links_nr = 1
    status_links_nr = 1
    rx_links_nr = 5
    links = []
    for i in range (tx_links_nr+status_links_nr+rx_links_nr):
        links.append(Worker())

    tx_queue = deque([])
    rx_queue = deque([])
    status_queue = deque([])

    logging.info ("CommsEngine <-> Cartarck link started at %s" % (time.ctime()))

    last_ct_update = 0
    last_online_report = 0
    last_ct_update_listen = 0
    last_queue_report = 0

    #poll_list = [comms_conn, ct_conn] + reduce(lambda x,y: x+y, map(lambda x: [x.comms_conn, x.ct_conn] if not (x.comms_conn.closed or tx.ct_conn.closed) else [], links))

    while True:

        if time.time() - last_online_report > ONLINE_REPORT_INTERVAL:
            comms_cur.execute("select * from se.device_ok_to_run('%s')" % (svc.device_name))
            if not comms_cur.fetchone()['device_ok_to_run']:
                logging.info ("Received termination signal from the DB at %s" % (time.ctime()))
                break
            last_online_report = time.time()

        try:
            #[rlist, wlist, xlist] = select.select ([comms_conn, ct_conn], [], [comms_conn, ct_conn], 5)
            poll_list = [comms_conn, ct_conn] + functools.reduce(lambda x,y: x+y, map(lambda x: [x.comms_conn, x.ct_conn] if not (x.comms_conn.closed or x.ct_conn.closed) else [], links))
            [rlist, wlist, xlist] = select.select (poll_list, poll_list, poll_list, 5)
            
            #poll workers
            for i in range(1, int(len(poll_list)/2)):
                try:
                    if poll_list[i*2].poll() == psycopg2.extensions.POLL_OK and poll_list[i*2+1].poll() == psycopg2.extensions.POLL_OK and links[i-1].busy:
                        links[i-1].poll_ok()
                except Exception as e:
                    logging.info ("Worker #%d failed to accomplish its task. The exception follows", (i-1))
                    logging.exception(e)
                    links[i-1].reconnect()
                
                
            if comms_conn not in rlist and ct_conn not in rlist == 0:
                logging.debug ("idle")
            else:
                if comms_conn in xlist:
                    reconnect_comms()
                if ct_conn in xlist:
                    reconnect_ct()
                
                if comms_conn in rlist or len(comms_conn.notifies):
                    comms_conn.poll()
                    l = len(comms_conn.notifies)
                    logging.debug ("%d new Commsengine notifications to process" % (l))
                    #l = min(l,10)
                    while comms_conn.notifies:
                    #for ii in range(l):
                        notify = comms_conn.notifies.pop()
                        if notify.channel == 'sm_rx':
                            rx_queue.append(notify.payload)
                        elif notify.channel == 'sm_tx':
                            status_queue.append(notify.payload)
                        elif notify.channel == 'update_replypath':
                            logging.debug ("Updating comms_ruleset table by signal %s" % (notify.channel))
                            comms_cur.execute ("select ruleset.*, se.get_driver_conf_option(driver.id, 'smsc') as smsc, " \
                                            "se.get_driver_conf_option(driver.id, 'reply_path') as device_reply_path " \
                                            "from se.ruleset, se.driver where driver_id=driver.id and ruleset.is_enabled and driver.is_enabled and driver.is_online " \
                                            "order by priority asc")
                            
                            ct_conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_READ_COMMITTED)
                            #ct_cur.execute ("truncate table tc.comms_ruleset")
                            ct_cur.execute ("delete from tc.comms_ruleset")
                            for r in comms_cur:
                                logging.debug ("Submitting rule %s" % (r['name']))
                                ct_cur.execute ("insert into tc.comms_ruleset " \
                                                "(id,name,prefix,driver_id,application_id,terminal_serial,priority,network_provider,smsc,reply_path,is_enabled) values " \
                                                "(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)", [ r['id'], r['name'], r['prefix'], r['driver_id'], r['application_id'], \
                                                r['terminal_serial'], r['priority'], r['network_provider'], \
                                                r['smsc'] if r['reply_path'] is not None else None, \
                                                r['device_reply_path'] if r['reply_path'] is None else r['reply_path'], r['is_enabled'] ])
                            ct_conn.commit()
                            ct_conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
                            

                if ct_conn in rlist or len(ct_conn.notifies):
                    ct_conn.poll()
                    l = len(ct_conn.notifies)
                    logging.debug ("%d new Cartrack notifications to process" % (l))
                    while ct_conn.notifies:
                        notify = ct_conn.notifies.pop()
                        #logging.debug ('==================================================> new notification: %s' % (notify.channel))
                        if notify.channel == 'notify_send_sms':
                            tx_queue.appendleft(['sms', notify.payload])
                        elif notify.channel == 'notify_send_autosms':
                            tx_queue.append(['autosms', notify.payload])
                        elif notify.channel == 'sm_to_client':
                            logging.debug("whats app message received!")
                            tx_queue.append(['socialmedia', notify.payload])
            #assign workers
            if time.time() - last_queue_report > QUEUE_REPORT_INTERVAL:
                logging.debug ("TX queue: %d, Status queue: %d, RX queue: %d" % (len(tx_queue), len(status_queue), len(rx_queue)))
                last_queue_report = time.time()
            
            for i in range(tx_links_nr):
                if len(tx_queue) == 0:
                    break
                if not links[i].busy:
                    [sm_type, payload] = tx_queue.popleft()
                    logging.debug ("Starting worker #%d for the notification (%s,%s)" % (i, sm_type, payload))
                    if sm_type == 'sms':
                        links[i].process_cartrack_tx(payload)
                    elif sm_type == 'autosms':
                        links[i].process_cartrack_autosms(payload)
                    elif sm_type == 'socialmedia':
                        links[i].process_cartrack_socialmedia(payload)

            for i in range(tx_links_nr, tx_links_nr+status_links_nr):
                if len(status_queue) == 0:
                    break
                if not links[i].busy:
                    payload = status_queue.popleft()
                    logging.debug ("Starting worker #%d for the notification %s" % (i, payload))
                    links[i].process_tx(payload)
            
            for i in range(tx_links_nr+status_links_nr, tx_links_nr+status_links_nr+rx_links_nr):
                if len(rx_queue) == 0:
                    break
                if not links[i].busy:
                    payload = rx_queue.popleft()
                    logging.debug ("Starting worker #%d for the notification %s" % (i, payload))
                    links[i].process_rx(payload)
        
            
            if int(time.time()) - last_ct_update > CT_UPDATE_INTERVAL:
                ct_cur.execute ("select * from ct.sp_application_stat(%s,%s,%s,%s)", ['CommsSync', 'CommsEngine', '1.0', local_address])
                last_ct_update = int(time.time())
            time.sleep(0.016)
            
        
        except psycopg2.Error as e:
            logging.exception(e)
            logging.error ("Error while sync'ing comms <-> cartrack:\n%s" % (e.pgerror))
            reconnect_comms()
            reconnect_ct()