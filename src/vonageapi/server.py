from hashlib import sha256
import json, jwt, logging

from twisted.web import http
from twisted.web.resource import Resource

class VonageServer (Resource):
    isLeaf = True
    
    def __init__(self, handler):
        Resource.__init__(self)
        self.handler = handler

    def render_GET (self, request):
        logging.debug ("Incoming GET request from %s" % request.getClientIP())
        logging.debug ("uri:%s, args:%s, headers: %s" % (request.uri, request.args, request.requestHeaders))

        request.setHeader("Content-Type", "application/json")
        request.setResponseCode(http.NOT_ALLOWED)
        return "".encode("utf-8")

    def render_POST (self, request):
        logging.debug ("Incoming POST request from %s" % request.getClientIP())
        logging.debug ("uri:%s, args:%s" % (request.uri, request.args))
        auth_header = request.getHeader('Authorization')
        if auth_header:
            auth_header = auth_header.replace("Bearer ", "")
        logging.debug("Auth header: %s" % auth_header)

        jwt_payload = {}
        try:
            jwt_payload = jwt.decode(auth_header, "Tgvnv126k6fxjsnnCtGWVMj4hrwgTKdsZlVhZ6ddn9rWyLPJTL", algorithms=["HS256"])
        except:
            logging.warn("signature verification failed!")
            request.setResponseCode(http.UNAUTHORIZED)

        if jwt_payload:
            logging.debug("signature verified: %s" % jwt_payload)
            str_body = request.content.read().decode("utf-8")
            logging.debug("request body: %s", str_body)
            data = json.loads(str_body)
            body_hash = sha256(str_body.encode("utf-8")).hexdigest()
            logging.debug("body_hash: %s" % body_hash)
            if body_hash == jwt_payload['payload_hash']:
                request_path = request.path.decode("utf-8").lower()
                logging.debug("path: %s" % request_path)
                if request_path == "/in_messages":
                    logging.debug("processing message")
                    request.setHeader("Content-Type", "application/json")
                    request.setResponseCode(http.OK)
                    if self.handler.process_message(data):
                        request.setResponseCode(http.OK)
                    else:
                        request.setResponseCode(http.BAD_REQUEST)
                elif request_path == "/status":
                    logging.debug("processing status")
                    self.handler.process_status(data)
                    request.setResponseCode(http.OK)
                else:
                    logging.debug("path not found")
                    request.setResponseCode(http.NOT_FOUND)
            else:
                logging.warn("payload verification failed")
                request.setResponseCode(http.UNAUTHORIZED)
        else:
            logging.warn("signature verification failed!")
            request.setResponseCode(http.UNAUTHORIZED)

        return "".encode("utf-8")