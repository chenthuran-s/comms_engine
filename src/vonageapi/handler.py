import datetime, logging, mimetypes, os, requests
from collections import deque

from twisted.web.server import Site
from twisted.web.static import File
from twisted.web.resource import Resource
from twisted.internet import ssl, reactor

import ct
from ct.database.ct_db_repo import CartrackDbRepo
from vonageapi.request import VonageSMSRequest, VonageQuickReplyRequest
from vonageapi.server import VonageServer

class VonageHandler:
    def __init__(self, comms_client, attachment_service):
        self.comms_ = comms_client
        self.attachment_service = attachment_service
        self.comms_.set_on_new_sm (self._on_new_sm)
        self.comms_.set_do_restart (self.restart)
        self.port_ = int (self.comms_.device.conf.get('listen_port', '81'))
        self.msg_queue = deque() 
        self.synchronise_interval = 30
        self.location_map = {}

    def start (self):
        try:
            ct_dsn = "dbname=%s host=%s user=%s password=%s" % (ct.conf['ct_db_name'], ct.conf['ct_db_host'], ct.conf['ct_db_user'], ct.conf['ct_db_pass'])
            self.ct_db = CartrackDbRepo(ct_dsn) 

            reactor.listenTCP (self.port_, Site (VonageServer(self)))

        except Exception as ex:
            logging.exception (ex)
            reactor.stop()
            return

        self.comms_.start()

    def restart(self):
        try:
            ct_dsn = "dbname=%s host=%s user=%s password=%s" % (ct.conf['ct_db_name'], ct.conf['ct_db_host'], ct.conf['ct_db_user'], ct.conf['ct_db_pass'])
            self.ct_db = CartrackDbRepo(ct_dsn)
            self.comms_.start() 
        except Exception as ex:
            logging.exception(ex)

    def _on_new_sm (self, smRow):
        logging.debug ("New request: sm_id: %u, target: %s" % (smRow['id'], smRow['target']))

        logging.debug("content: %s", smRow['data'].tobytes().decode("utf-8"))
        VonageSMSRequest (self, smRow['id'], smRow['target'], smRow['data']).start()

    def process_message(self, data):
        if not self.ct_db.is_connected():
            self.ct_db.connect_database()

        msisdn = data.get("from", {}).get("number", "")
        msg_type = data.get("from", {}).get("type", "")
        content = data.get("message", {}).get("content", {})
        msg_id = data.get("message_uuid", "")

        if msisdn and msg_type and content:
            content_type = content.get("type", "")
            if content_type == "text":
                msg = content.get("text", "")
                logging.debug("Processing inbound whatsapp text message\nmsisdn: {}, type: {}".format(msisdn, msg_type))
                self.ct_db.add(msisdn, msg, msg_type)
                return True
            elif content_type.lower() in ["image", "video", "audio", "file"]:
                logging.debug("Processing inbound whatsapp attachment message\nmsisdn: {}, type: {}".format(msisdn, msg_type))
                token = self.comms_.device.conf.get("token")
                image = content.get(content_type, {})
                if image:
                    url = image.get("url", None)
                    if url:
                        caption = image.get("caption", None)
                        logging.debug("downloading content from url: %s" % url)
                        headers = {
                            'Authorization': 'Basic ' + token
                        }
                        response = requests.get(url, headers=headers)
                        if response.status_code == 200:
                            logging.debug("%s bytes downloaded", str(len(response.content)))
                            sm_id = self.ct_db.update_messages(msisdn, 'File received, uploading to the server now...', msg_type)
                            res_content_type = response.headers['Content-Type']
                            logging.debug("content_type: %s, message_type: %s" % (res_content_type, content_type))
                            file_ext = mimetypes.guess_extension(res_content_type)
                            if file_ext == None:
                                if content_type == 'audio':
                                    file_ext = ".mp3"
                                elif content_type == 'video':
                                    file_ext = ".mp4"
                                elif content_type == 'image':
                                    file_ext = ".jpeg"
                                else:
                                    file_ext = ""

                            logging.debug("file_ext: %s" % file_ext)
                            filename = "{}{}".format(msg_id, file_ext)
                            result = self.attachment_service.upload(sm_id, response.headers['Content-Type'], response.content, filename)
                            if result is not None:
                                try:
                                    self.ct_db.add_to_attachments('social_media', sm_id, filename, result)
                                except Exception as ex:
                                    logging.debug("failed to update ct.attachment table")
                                    logging.debug(ex)

                                upd_msg = result
                                if caption:
                                    upd_msg = "{}\n{}".format(upd_msg, caption)
                                upd_msg = "{}\nDouble click here to view the file".format(upd_msg)    
                                self.ct_db.update_message(sm_id, upd_msg)
                            else:
                                self.ct_db.update_message(sm_id, "File upload failed")
                        else:
                            logging.warn("Could not download the attachment content")
                    else:
                        logging.warn("Image url was not found")
                else:
                    logging.warn("Image url was not found")

                return True
            elif content_type == "location":
                location = content.get("location", None)
                if location:
                    # logging.info(str(location))
                    # text = "https://maps.google.com/?q=" + str(location.get('lat', 0)) + "," + str(location.get('long', 0)) + "\nThe client has shared a location"
                    # self.ct_db.update_messages (msisdn,text)

                    # client_id = self.ct_db.get_client_id(msisdn)
                    # if client_id:
                    #     self.location_map[str(client_id)] = location
                    #     logging.info("client id: %s" % client_id)
                    #     VonageQuickReplyRequest (self, 0, msisdn, "").start()
                    # else:
                    #     logging.info("%s was not identified as a valid client" % msisdn)
                    logging.info("skipping location share")
                else:
                    logging.info("Unable to get the location details. The request was not processed")

                return True
            elif content_type == "button":
                button_text = content.get("button", {}).get("text")
                if button_text:
                    self.ct_db.add(msisdn, button_text, msg_type)
                    if button_text == "Change delivery date":
                        client_id = self.ct_db.get_client_id(msisdn)
                        location = self.location_map.get(str(client_id), {})
                        if client_id and location:
                            logging.info("creating a control room case")
                            if self.ct_db.create_case(client_id, location['lat'], location['long']):
                                logging.info("case created successfully")
                            else:
                                logging.info("failed to create a case")
                        else:
                            logging.info("client id / location was unavailable. failed to create a control room case")
                    else:
                        logging.info("No further action needed")
                    return True
                else:
                    logging.info("No text found, the request was not processsed")
                    return True
            else:
                logging.info("Content type '{}' is not supported. The request was not processed".format(content_type))
                return True

        else:
            logging.warn("The from number / from type / content was empty. The request was not processed")
            return True


    def on_sm_accepted(self, sm_tx_queue_id, uuid):
        logging.debug("the send message request was accepted for %s" % sm_tx_queue_id)
        self.ct_db.update_msg_uuid(sm_tx_queue_id, uuid)
        self.comms_.sm_sent (sm_tx_queue_id, uuid)


    def process_status(self, data):
        uuid = data.get("message_uuid", "")
        status = data.get("status", "").lower()

        if status == "submitted":
            self.ct_db.update_msg_state(uuid, 3)
        elif status == "delivered":
            self.ct_db.update_msg_state(uuid, 4)
        elif status == "rejected":
            error_reason = data.get("error", {}).get("reason", "")
            self.ct_db.update_msg_state(uuid, 6, error_reason)
        else:
            logging.debug("'%s' status skipped" % status)