import logging, traceback,sys
from base64 import b64encode

from twisted.web import http
from twisted.web.http_headers import Headers

from ct.web.request import HTTPRequest
import json

class VonageRequest (HTTPRequest):
    def __init__(self, url, token, on_sent, on_fail):
        HTTPRequest.__init__(self, on_sent, on_fail)
        self.url = url
        self.token = token 
    
    def get_method(self):
        return "POST".encode("utf-8")
    
    
    def get_headers(self):
        return Headers ({'Content-Type': ['application/json'], 'authorization': ["Basic " + self.token]})
    
    def get_url(self):
        return self.url.encode("utf-8")


class VonageSMSRequest (VonageRequest):
    def __init__(self, handler, sm_tx_queue_id, sm_target, sm_data):
        self.handler = handler
        self.sm_tx_queue_id = sm_tx_queue_id
        self.sm_target = sm_target
        self.sm_data = sm_data
        self.response_code = None
        self.error_msg = None
        url = handler.comms_.device.conf.get('submit_url', '')
        token = handler.comms_.device.conf.get('token')
        self.from_number = handler.comms_.device.conf.get("from_number")
        self.message_type = handler.comms_.device.conf.get("message_type")
        VonageRequest.__init__(self, url, token, self._request_ok, self._request_failed)
    
    def _request_ok (self, response_code):
        logging.debug ("HTTP response code %d for %s" % (response_code, self.sm_target))
        self.response_code = response_code
    
    def _request_failed (self):
        logging.debug ("HTTP request failed for %s with error %s" % (self.sm_target, self.error_msg))
        if (self.sm_tx_queue_id is not None):
            self.handler.on_sms_request_failed (self.sm_tx_queue_id, None, 'HTTP request failed: ' + self.error_msg)

    def parse_response (self, resp):
        try:
            logging.debug ("Response (%s, %s): %s" % (self.response_code, type(resp), resp))
            content = json.loads(resp)
            if self.response_code == http.ACCEPTED or self.response_code == http.OK:
                uuid = content.get("message_uuid", None)
                if uuid:
                    self.handler.on_sm_accepted(self.sm_tx_queue_id, uuid)
                return

            if content.get ("message"):
                self.error_msg = content["message"]
            else:
                self.error_msg = "No error message available"
    
            self._request_failed()
        except:
            traceback.print_exception(*sys.exc_info())

    def get_request_body (self):
        msg = self.sm_data.tobytes().decode("utf-8")
        logging.debug("msg type: %s" % type(msg))
        formatted_payload = None
        try:
            formatted_payload = json.loads(msg)
            logging.debug("deserialized json")
        except:
            logging.debug("failed to deserialize as json")

        data = None
        if formatted_payload and type(formatted_payload) is dict and formatted_payload.get("content", None):
            data = {
                "from": { "type": self.message_type, "number": self.from_number },
                "to": { "type": self.message_type, "number": self.sm_target.replace('{}:'.format(self.message_type), '') },
                "message": formatted_payload
            }
        else:
            data = {
                "from": { "type": self.message_type, "number": self.from_number },
                "to": { "type": self.message_type, "number": self.sm_target.replace('{}:'.format(self.message_type), '') },
                "message": {
                    "content": {
                        "type": "text",
                        "text": msg
                    }
                }
            }

        return json.dumps(data).encode("utf-8")


class VonageQuickReplyRequest (VonageRequest):
    def __init__(self, handler, sm_id, sm_target, sm_data):
        self.handler = handler
        self.sm_id = None
        self.sm_target = sm_target
        self.sm_data = sm_data
        self.response_code = None
        self.error_msg = None
        url = handler.comms_.device.conf.get('submit_url', '')
        token = handler.comms_.device.conf.get('token')
        self.from_number = handler.comms_.device.conf.get("from_number")
        self.message_type = handler.comms_.device.conf.get("message_type")
        VonageRequest.__init__(self, url, token, self._request_ok, self._request_failed)
    
    def _request_ok (self, response_code):
        logging.debug ("HTTP response code %d for %s" % (response_code, self.sm_target))
        self.response_code = response_code
    
    def _request_failed (self):
        logging.debug ("HTTP request failed for %s with error %s" % (self.sm_target, self.error_msg))
        if (self.sm_id is not None):
            self.handler.on_sms_request_failed (self.sm_id, None, 'HTTP request failed: ' + self.error_msg)

    def parse_response (self, resp):
        logging.debug ("Response: %s" % resp)
        if (self.response_code == http.OK): 
            return
        content = json.loads(resp)
        if content.get ("message"):
            self.error_msg = content["message"]
        else:
            self.error_msg = "No error message available"
 
        self._request_failed()

    def get_request_body (self):
        # msg = self.sm_data.tobytes().decode("utf-8")
        data = {
            "from": { "type": self.message_type, "number": self.from_number },
            "to": { "type": self.message_type, "number": self.sm_target.replace('{}:'.format(self.message_type), '') },
            "message": {
                "content": {
                    "type": "custom",
                    "custom": {
                        "type": "template",
                        "template": {
                            "namespace": "9b6b4fcb_da19_4a26_8fe8_78074a91b584",
                            "name": "sandbox_shipping_update",
                            "language": {
                                "code": "en_GB",
                                "policy": "deterministic"
                            },
                            "components": [
                                {
                                    "type": "body",
                                    "parameters": [
                                        {
                                            "type": "text",
                                            "text": "Darth Vader"
                                        },
                                        {
                                            "type": "date_time",
                                            "date_time": {
                                                "fallback_value": "February 25, 1977",
                                                "day_of_week": 5,
                                                "day_of_month": 25,
                                                "year": 1977,
                                                "month": 2,
                                                "hour": 15,
                                                "minute": 33
                                            }
                                        },
                                        {
                                            "type": "text",
                                            "text": "10:00 am"
                                        },
                                        {
                                            "type": "text",
                                            "text": "3:00 pm"
                                        }
                                    ]
                                }
                            ]
                        }
                    }
                }
            }
        }

        return json.dumps(data).encode("utf-8")