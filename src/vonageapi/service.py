import logging
from twisted.internet import reactor

from ct.attachments.attachment_service import get_attachment_service
from ct.commsengine.client import Client
from ct.service import ServiceBase
from vonageapi.handler import VonageHandler

class VonageClient (Client):
    def __init__(self, device_name, dsn):
        Client.__init__(self, device_name, dsn)

def main():
    logging.basicConfig (format='%(levelname)s:%(asctime)s:%(message)s', datefmt='%m/%d/%Y %H:%M:%S', level=logging.DEBUG)
    logging.info("Starting the Cartrack Chat Service")

    svc = ServiceBase()
    logging.info(svc.__dict__)

    try:
        comms_client = VonageClient(svc.device_name, "dbname=%s host=%s user=%s password=%s" % (svc.conf['db_name'], svc.conf['db_host'], svc.conf['db_user'], svc.conf['db_pass']))
        attachment_service = get_attachment_service(svc.conf['attachment_service_name'])

        h = VonageHandler(comms_client, attachment_service)
        reactor.callWhenRunning(h.start)
        logging.info("starting reactor")
        reactor.run()
        logging.info("over and out")
    except Exception:
        logging.exception('error')