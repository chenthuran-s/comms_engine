import logging

from twisted.internet import reactor
from twisted.web.client import Agent
from twisted.web.client import ResponseDone
from twisted.web.http_headers import Headers
from twisted.internet.defer import Deferred
from twisted.internet.protocol import Protocol
from twisted.web.iweb import IBodyProducer
from twisted.internet.defer import succeed


class ResponseFetcher(Protocol):
    def __init__(self, finished):
        self.finished = finished
        self.buf = ''
    
            
    def dataReceived(self, bytes):
        self.buf += bytes.decode("utf-8")
    
    
    def connectionLost(self, reason):
        self.finished.callback(self.buf)


class StringProducer(object):
    def __init__(self, body):
        self.body = body
        self.length = len(body)


    def startProducing(self, consumer):
        consumer.write(self.body)
        return succeed(None)


    def pauseProducing(self):
        pass


    def stopProducing(self):
        pass

                                                                
class HTTPRequest:

    def __init__(self, on_request_sent=None, on_request_failed=None):
        self._on_request_sent = on_request_sent
        self._on_request_failed = on_request_failed
    
    
    def start(self):
        factory = self.get_endpoint_factory()
        if factory is not None:
            agent = Agent.usingEndpointFactory(reactor, factory)
        else:
            agent = Agent(reactor)
        body = self.get_request_body()
        url = self.get_url()
        logging.debug ("New request to %s; body: %s"  % (url, body))
        # logging.debug ("New request to %s (%s)"  % (url, type(url)))
        d = agent.request(self.get_method(), url, self.get_headers(), 
                          None if body is None else StringProducer(body))
        
        d.addCallbacks(self.request_sent, self.request_error)
    
    
    def request_sent(self, r):
        if self._on_request_sent:
            self._on_request_sent (r.code)
        finished = Deferred()
        finished.addCallbacks(self.response_received, self.request_error)
        r.deliverBody(ResponseFetcher(finished))
        return finished
    
    
    def request_error(self, ex):
        logging.debug ("Request failed: %s" % (ex.getErrorMessage()))
        logging.exception(ex)
        if self._on_request_failed:
            self._on_request_failed()
    
    
    def response_received(self, msg):
        logging.debug ("Incoming reply of length %d" % (len(msg)))
        self.parse_response (msg)


    def get_url(self):
        logging.debug ("This is an abstract get_request()")
        return None
        
    
    def parse_response(self, resp):
        logging.debug ("This is an abstract parse_response()")
        return None
    
    
    def get_request_body(self):
        return None
    
    
    def get_method(self):
        return "GET"
        
    
    def get_headers(self):
        return Headers({'User-Agent': ['Cartrack web client']})
    
    def get_endpoint_factory(self):
        return None
