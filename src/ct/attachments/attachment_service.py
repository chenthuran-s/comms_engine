import abc
import boto3
import botocore
import logging

import ct

ABC = abc.ABCMeta('ABC', (object,), {'__slots__': ()}) 

class AttachmentService(ABC):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'upload') and
                callable(subclass.upload))


def get_attachment_service(service_name):
    if service_name == "minio":
        return MinIOService()
    else:
        logging.debug("Service not found: %s" % service_name)
        return None

class MinIOService:

    def __init__(self):
        self.client = boto3.client(
            "s3",
            aws_access_key_id=ct.conf['minio_access_key'],
            aws_secret_access_key=ct.conf['minio_secret_key'],
            endpoint_url="https://{}".format(ct.conf['minio_host'])
        )

    def upload(self, sm_id, content_type, content_bytes, filename):
        object_key = "social_media/{}/{}".format(sm_id,filename)
        logging.debug("minio upload: https://%s/%s/social_media/%s/%s" % (ct.conf['minio_host'], ct.conf['minio_bucket_name'], sm_id, filename))
        self.client.put_object(ContentType=content_type, Body=content_bytes, Bucket=ct.conf['minio_bucket_name'], Key=object_key)
        try:
            self.client.head_object(Bucket=ct.conf['minio_bucket_name'], Key=object_key)
            logging.debug("minio upload succesful")
            return "minio://{}/{}".format(ct.conf['minio_bucket_name'], object_key)
        except Exception:
            logging.debug("File upload failed")

        return None
