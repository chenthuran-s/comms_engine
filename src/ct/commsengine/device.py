import json, psycopg2, psycopg2.extras, logging, sys

class Device:
    
    def __init__(self, device_name, dbh):
        self.device_name = device_name
        self.dbh = dbh
        self._load()
    
    
    def _load(self):
        sql = 'select * from se.get_driver(%s)'
        args = (self.device_name,)
        sth = self.dbh.cursor(cursor_factory=psycopg2.extras.DictCursor)
        logging.debug(sth.mogrify(sql, args))
        sth.execute(sql, args)
        r = sth.fetchone()
        self.id = r['id']
        try:
            self.conf = json.loads(r['config'])
            # self.conf = cjson.decode(r['config'])
            # self.conf['message_templates'] = temp_conf.get("message_templates")
        except Exception as ex:
            logging.error('Failed to decode json config for device %s: %s' % (self.device_name, ex))
            sys.exit(-1)
        
        logging.info('loaded config for device %s: %s', self.device_name, self.conf)
    
    
    def mark_online(self):
        sql = 'select se.driver_online(%s)'
        args = (self.id,)
        sth = self.dbh.cursor()
        logging.debug(sth.mogrify(sql, args))
        sth.execute(sql, args)
    
    
    def mark_offline(self):
        sql = 'select se.driver_offline(%s)'
        args = (self.id,)
        sth = self.dbh.cursor()
        logging.debug(sth.mogrify(sql, args))
        sth.execute(sql, args)


    def ok_to_run(self):
        sql = 'select se.device_ok_to_run(%s)'
        args = (self.device_name,)
        sth = self.dbh.cursor()
        logging.info(sth.mogrify(sql, args))
        sth.execute(sql, args)
        r = sth.fetchone()
        if not r[0]:
            logging.error('device not ok to run; another handler is active for it or it\'s disabled; exit')
            sys.exit(-1)
