import logging
from twisted.internet.abstract import FileDescriptor, main
from twisted.internet import reactor

class pglistener(FileDescriptor):
    
    def __init__(self, dbh):
        FileDescriptor.__init__(self)
        self.dbh_ = dbh
        self.on_read_ = None
        self.startReading()
    
    
    def doRead(self):
        self.dbh_.poll()
        while self.dbh_.notifies:
            n = self.dbh_.notifies.pop()
            #logging.debug('got notify: bepid=%u channel=%s payload=%s' % (n.pid, n.channel, repr(n.payload)))
        
        if self.on_read_:
            try:
                self.on_read_()
            except Exception:
                logging.exception('considering db connection closed')
                reactor.stop()
    
    
    def setOnRead(self, cb):
        self.on_read_ = cb
    
    
    def readConnectionLost(self, reason):
        logging.error('disconnected from db: %s' % repr(reason))
        reactor.stop()


    def fileno(self):
        return self.dbh_.fileno()
