from collections import deque
import time
import logging

class speed_controller:

    def __init__(self, max_speed):
        self.set_speed(max_speed)
        self.q = deque ([0 for i in range(3600)], 3600)
        self.last_second = int(time.time())

        self.last_speed = -1
        self.last_speed_tm = -1
    
    
    def set_speed(self, max_speed):
        self.max_speed_pm = max_speed
        self.max_speed_ps = max(1, max_speed / 3600.0)
        self.max_msg_interval = max(1, 3600 / max_speed)
        
    
    def interval_count(self, interval):
        tm = int(time.time())
        last_sec = interval - (tm - self.last_second)
        i = 3600 - last_sec
        cnt = 0
        while i < 3600:
            cnt += self.q[i]
            i += 1        
        return cnt

    
    def hit(self):
        tm = int(time.time())
        if tm == self.last_second:
            self.q[-1] += 1
        else:
            for i in range(min(3600, tm - self.last_second - 1)):
                self.q.append(0)
            self.q.append(1)
            self.last_second = tm
        #logging.debug ("SpeedControl: Hit. Current overall sum is %d" % sum(self.q))
    
    
    def speed_exceeded(self):
        if self.speed() <= self.max_speed_pm and self.q[-1] <= self.max_speed_ps:
            return False
        return True
    
    
    def momentum_quota(self):
        logging.debug ("SpeedControl speed: %d" % self.speed())
        logging.debug ("SpeedControl max_pm: %d" % self.max_speed_pm)
        q = int(min(self.max_speed_pm - self.speed(), self.max_speed_ps if int(time.time()) != self.last_second else self.max_speed_ps - self.q[-1], self.max_speed_pm if self.max_msg_interval <= 1 else 1 - self.interval_count(self.max_msg_interval)))
        return q if q >= 0 else 0
    
    
    def speed(self):
        tm = int(time.time())
        #if tm != self.last_speed_tm:
        cnt = 0
        i = tm - self.last_second
        #logging.debug ("SpeedControl: skipping first %d secs for speedcount" % i)
        #logging.debug ("SpeedControl: Current overall sum is %d" % sum(self.q))
        while i < 3600:
            cnt += self.q[i]
            i += 1
        self.last_speed = cnt
        self.last_speed_tm = int(time.time())
        #print self.q

        return self.last_speed
    
    
    def get_last_hit_tm(self):
        return self.last_second
