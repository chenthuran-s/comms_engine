import string

def dump_string(s):
    rv = "%s" % (''.join(map(lambda x: x if x in string.printable and ord(x) > 20 else "[%02x]" % (ord(x)),s)))
    return rv.replace("\r", "\\r").replace("\n", "\\n")

def dump_bytea(s):
    rv = "%s" % (''.join(map(lambda x: chr(x) if chr(x) in string.printable and (x > 20 or x == 10 or x == 13) else "[%02x]" % (int(x)),s)))
    return rv.replace("\r", "\\r").replace("\n", "\\n")
