from datetime import datetime, timedelta
import psycopg2, logging
from twisted.internet import reactor, defer
from twisted.python.failure import Failure
import time

from ct.commsengine.device import Device
from ct.commsengine.pglistener import pglistener
from ct.commsengine.speed_control import speed_controller
from ct.commsengine import misc


class Client:
    
    def __init__(self, device_name, dsn):
        # connect to db
        self.dsn = dsn
        logging.info('connecting to commsengine db with dsn=%s', dsn)
        dbh = psycopg2.connect(dsn)
        dbh.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        logging.info('connected to commsengine db')
        self.dbh = dbh
        
        self.do_stuff_timer_ = None
        
        # setup device
        self.device = Device(device_name, dbh)
        self.device.ok_to_run()
        self.on_new_sm_ = None
        
        sth = self.dbh.cursor()
        sth.execute('listen sm_tx')
        
        self.speed_control = None
        self.target_speed_control = None
        self.__read_settings()
        self.sm_limit_ = self.sm_limit_max_  # current limit; gets updated within [0 .. sm_limit_max] range
        
        self.last_sm_fetch_tm = 0
        self.last_config_read_tm = time.time()
        self.active_queue = []
        logging.debug('init: concurrent_sm_max=%u' % self.sm_limit_max_)

        # Adding for python 3 complaint in VS Code
        self.last_mark_online_at_ = None
        self.last_config_read_at_ = None
        self.last_sm_fetch_limit_at_ = None
        self.last_cleanup_speed_limits_at_ = None

        self.do_restart_ = None

    def __reconnect(self):
        logging.info('reconnecting to commsengine db with dsn=%s', self.dsn)
        dbh = psycopg2.connect(self.dsn)
        dbh.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
        logging.info('connected to commsengine db')
        self.dbh = dbh
        
        sth = self.dbh.cursor()
        sth.execute('listen sm_tx')
    
    def __read_settings(self):
        self.sm_limit_max_ = int(self.device.conf.get('concurrent_sm_max', '20')) # remember this limit
        self.sm_fetch_cooldown_period = int(self.device.conf.get('fetch_cooldown_period', '0')) # min delay between sms fetch
        self.max_speed_per_hour = int(self.device.conf.get('max_speed_per_hour', '-1'))
        self.max_target_speed_per_hour = int(self.device.conf.get('max_target_speed_per_hour', '-1'))
        self.last_sm_fetch_limit = self.sm_limit_max_
        
        if self.max_speed_per_hour > 0:
            if self.speed_control is None:
                self.speed_control = speed_controller(self.max_speed_per_hour)
            else:
                self.speed_control.set_speed(self.max_speed_per_hour)
            logging.debug('init: max delivery speed per hour=%u' % self.max_speed_per_hour)
        else:
            self.speed_control = None
        
        if self.max_target_speed_per_hour > 0:
            if self.target_speed_control is None:
                self.target_speed_control = {}
            else:
                for sc in self.target_speed_control.values():
                    sc.set_speed(self.max_target_speed_per_hour)
            logging.debug('init: max delivery speed per hour per target=%u' % self.max_target_speed_per_hour)
        else:
            self.target_speed_control = None
        
    
    def start(self):
        self.pgl_ = pglistener(self.dbh)
        self.pgl_.setOnRead(self.__db_read_notified)
        self.__schedule_do_stuff(0)
    
    
    def __do_stuff(self):
        logging.debug('ct.commsengine.client: do_stuff')
        carry_on = False

        # Added the code below to catch situations where the database
        # connection that this class holds has dropped
        # The try catch below is a quick fix.
        # Have to find a better way to implement this
        try:
            sth = self.dbh.cursor()
            sth.execute('select 1')
            carry_on = True
        except Exception as ex:
            logging.exception(ex)
            logging.warning("Database connection failed. Trying to reconnect...")

        if carry_on:
            self.__schedule_do_stuff(1) # do this early: don't let exceptions from the code that follows disrupt this
            self.__mark_online_if()
            self.__reload_config_if()
            self.__save_last_sm_fetch_limit_if()
            self.__cleanup_old_target_speed_limits()
            self.__get_next_sm()
        else:
            try:
                self.__reconnect()
                self.do_restart_()
            except Exception as ex:
                logging.exception(ex)
                self.__schedule_do_stuff(1)
    
    def __mark_online_if(self):
        if self.last_mark_online_at_ is not None and self.last_mark_online_at_ + timedelta(minutes=1) > datetime.utcnow():
            return
        
        self.last_mark_online_at_ = datetime.utcnow()
        self.device.mark_online()
    
    
    def __reload_config_if(self):
        if self.last_config_read_at_ is not None and time.time() - self.last_config_read_at_ < 60*10:
            return
        self.last_config_read_at_ = time.time()
        
        self.device._load()
        self.__read_settings()
        

    def __save_last_sm_fetch_limit_if(self):
        if self.last_sm_fetch_limit_at_ is not None and time.time() - self.last_sm_fetch_limit_at_ < 60:
            return
        self.last_sm_fetch_limit_at_ = time.time()
        self.save_last_sm_fetch_limit()


    def __cleanup_old_target_speed_limits(self):
        if (self.target_speed_control is None 
            or (hasattr(self, 'last_cleanup_speed_limits_at_') and time.time() - self.last_cleanup_speed_limits_at_ < 60)):
            return
        
        tm = time.time()
        self.last_cleanup_speed_limits_at_ = tm
        for k in self.target_speed_control.keys():
            if tm - self.target_speed_control[k].get_last_hit_tm() > 3600:
                #older than hour? skipping
                del self.target_speed_control[k]

            
    def __db_read_notified(self):
        if time.time() - self.last_sm_fetch_tm > self.sm_fetch_cooldown_period:
            self.__get_next_sm()
            self.last_sm_fetch_tm = time.time()

    
    def __get_next_sm(self):
        if not self.on_new_sm_:
            logging.error('"new_sm" callback not defined => unable to fetch sm')
            return
        if self.sm_limit_ <= 0:
            logging.debug('sm_limit=%d <= 0 => bail' % self.sm_limit_)
            return

        speed_limit = self.sm_limit_
        if self.speed_control is not None:
            speed_limit = min(speed_limit, self.speed_control.momentum_quota())
            if speed_limit <= 0:
                logging.debug ('Max speed exceeded => bail')
                return
        
        sth = self.dbh.cursor(cursor_factory=psycopg2.extras.DictCursor)
        sql = 'select * from se.get_next_sm(%s,%s)'
        get_nr = min(self.sm_limit_, speed_limit)
        args = (self.device.id, get_nr)
        logging.debug(sth.mogrify(sql, args))
        sth.execute(sql, args)

        self.last_sm_fetch_limit = get_nr
        
        if not sth.rowcount and not len(self.active_queue):
            return
        
        logging.debug("got %u sm from db. Already preserving %d sm by speedcontrol" % (sth.rowcount, len(self.active_queue)))
        #self.sm_limit_ -= sth.rowcount
        speed_limit -= sth.rowcount
        
        local_target_limit = {}
        
        while True:
            smRow = sth.fetchone()
            if not smRow:
                break
            self.active_queue.append(smRow)
            logging.debug ("Fetched sm id: %d" % smRow['id'])
        
        l = len(self.active_queue)
        i = 0
        while i < l:
            smRow = self.active_queue[i]
            logging.debug ("Checking sm id: %d" %  smRow['id'])
            
            if self.target_speed_control is not None:
                target = smRow['target']
                if target in self.target_speed_control:
                    logging.debug ('Speed limit check for: %s. Got %d' % (target, self.target_speed_control[target].momentum_quota()))
                
                if target not in local_target_limit:
                    if target in self.target_speed_control:
                        local_target_limit[target] = self.target_speed_control[target].momentum_quota()
                    else:
                        local_target_limit[target] = max(1, self.max_target_speed_per_hour / 3600)
                
                if local_target_limit[target] <= 0:
                    logging.debug ('Speed limit check for: %s. Got %d' % (target, local_target_limit[target]))
                    i += 1
                    continue
                local_target_limit[target] -= 1
                
            try:
                del self.active_queue[i]
                l -= 1
                self.sm_limit_ -= 1
                self.on_new_sm_(smRow)
            except Exception as ex:
                logging.exception('submit failed')
                self.sm_failed(smRow['id'], None, ex)
        
        logging.debug('get_nr=%u got=%u sm_limit=%u speed_quota=%s' % (get_nr, sth.rowcount, self.sm_limit_, 'undef' if self.speed_control is None else str(speed_limit)))
        
        if get_nr == sth.rowcount and self.sm_limit_ > 0:
            logging.debug('has capacity to send more, call myself in a bit')
            self.__schedule_do_stuff(0)
        elif len(self.active_queue) > 0:
            self.__schedule_do_stuff(5)
    
    
    def set_on_new_sm(self, cb): self.on_new_sm_ = cb

    def set_do_restart(self, cb): self.do_restart_ = cb
    
    
    def __up_limit(self):
        if self.sm_limit_ == 0: # got one free slot
            logging.debug('got one free slot, reschedule fetch in 1 second')
            self.__schedule_do_stuff(1)
        
        self.sm_limit_ += 1
        if self.sm_limit_ > self.sm_limit_max_:
            logging.debug('self.sm_limit_=%u > self.sm_limit_max_=%u => cap back to %u' % (self.sm_limit_, self.sm_limit_max_, self.sm_limit_max_))
            self.sm_limit_ = self.sm_limit_max_
    
    
    def tx_attempt(self, target):
        logging.debug ('TX attempt for %s' % target)
        if self.speed_control is not None:
            self.speed_control.hit()
        
        if self.target_speed_control is not None:
            if target not in self.target_speed_control:
                self.target_speed_control[target] = speed_controller(self.max_target_speed_per_hour)
            self.target_speed_control[target].hit()


    def sm_sent(self, sm_id, drv_sm_id):
        sql = 'select se.sm_sent(%s,%s,%s::varchar::bytea)'
        args = (self.device.id, sm_id, drv_sm_id)
        sth = self.dbh.cursor()
        logging.debug(sth.mogrify(sql, args))
        sth = self.dbh.cursor()
        sth.execute(sql, args)
        if sm_id is not None:
            self.__up_limit()
    
    
    def sm_failed(self, sm_id, drv_sm_id, reason):
        sql = 'select se.sm_failed(%s,%s,%s::varchar::bytea,%s)'
        args = (self.device.id, sm_id, drv_sm_id, str(reason))
        sth = self.dbh.cursor()
        logging.debug(sth.mogrify(sql, args))
        try:
            sth.execute(sql, args)
        except Exception as e:
            new_reason = "Failed to save failure reason: %s" % str(e)
            args = (self.device.id, sm_id, drv_sm_id, str(new_reason))
            logging.debug(sth.mogrify(sql, args))
            sth.execute(sql, args)
        if sm_id is not None:
            self.__up_limit()
    
    
    def sm_delivered(self, sm_id, drv_sm_id):
        sql = 'select se.sm_delivered(%s,%s,%s::varchar::bytea)'
        args = (self.device.id, sm_id, drv_sm_id)
        sth = self.dbh.cursor()
        logging.debug(sth.mogrify(sql, args))
        sth.execute(sql, args)
        
        #NOTE: handlers aren't supposed to wait for delivery receipts to release delivery task, so no need to __up_limit() here
    
    
    def sm_defer(self, sm_id):
        sql = 'select se.sm_defer(%s,%s)'
        args = (self.device.id, sm_id)
        sth = self.dbh.cursor()
        logging.debug(sth.mogrify(sql, args))
        sth.execute(sql, args)
        if sm_id is not None:
            self.__up_limit()
    
    
    def sm_resend_all_for_target(self, target):
        sql = 'select se.sm_resend(%s,%s)'
        args = (self.device.id, target)
        sth = self.dbh.cursor()
        logging.debug(sth.mogrify(sql, args))
        sth.execute(sql, args)
        
        # call do_stuff as quickly as possible
        self.__schedule_do_stuff(0)
    
    
    def __schedule_do_stuff(self, timeout):
        if self.do_stuff_timer_ and self.do_stuff_timer_.active():
            self.do_stuff_timer_.reset(timeout)
        else:
            self.do_stuff_timer_ = reactor.callLater(timeout, self.__do_stuff)
    
    
    def sm_deliver(self, data, sender, target, sent_epoch=None, terminal_serial=None, message_channel=None):
        sql = 'select se.sm_deliver(%s,%s,now(),COALESCE(to_timestamp(%s),now()),%s,%s,%s,%s)'
        args = (self.device.id, memoryview(data), sent_epoch, sender, target, terminal_serial, message_channel)
        sth = self.dbh.cursor()
        logging.debug(misc.dump_string(sth.mogrify(sql, args)))
        sth.execute(sql, args)

    
    def set_target_auth_granted(self, target):
        sql = 'select se.set_target_auth_granted(%s,%s)'
        args = (self.device.id, target)
        sth = self.dbh.cursor()
        logging.debug(sth.mogrify(sql, args))
        sth.execute(sql, args)


    def set_target_auth_denied(self, target):
        sql = 'select se.set_target_auth_denied(%s,%s)'
        args = (self.device.id, target)
        sth = self.dbh.cursor()
        logging.debug(sth.mogrify(sql, args))
        sth.execute(sql, args)


    def set_target_auth_requested(self, target):
        sql = 'select se.set_target_auth_requested(%s,%s)'
        args = (self.device.id, target)
        sth = self.dbh.cursor()
        logging.debug(sth.mogrify(sql, args))
        sth.execute(sql, args)
    
    
    def get_target_auth_state(self, target):
        sql = 'select se.get_target_auth_state(%s, %s)'
        args = (self.device.id, target)
        sth = self.dbh.cursor()
        logging.debug(sth.mogrify(sql, args))
        sth.execute(sql, args)
        r = sth.fetchone()
        return r[0]
    
    
    def sm_reschedule(self, sm_id, reason): #TEST
        sql = 'select se.sm_reschedule(%s,%s,False,%s)'
        args = (self.device.id, sm_id, str(reason))
        sth = self.dbh.cursor()
        logging.debug(sth.mogrify(sql, args))
        sth.execute(sql, args)
        if sm_id is not None:
            self.__up_limit()
        is_ok = sth.fetchone()[0]
        if is_ok:
            logging.debug('resheduled for another attempt')
        else:
            logging.debug('this was final attempt, marked as failed')
    
    
    def save_last_sm_fetch_limit(self):
        sql = 'select se.update_device_state(%s,%s)'
        args = (self.device.id, self.last_sm_fetch_limit)
        sth = self.dbh.cursor()
        logging.debug(sth.mogrify(sql, args))
        sth.execute(sql, args)

