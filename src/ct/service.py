import json, logging, os, sys

import ct

class ServiceBase:
    def __init__(self):
        logging.basicConfig (format='%(levelname)s:%(asctime)s:%(message)s', datefmt='%m/%d/%Y %H:%M:%S', level=logging.DEBUG)

        if len (sys.argv) < 2:
            logging.error('ERROR: device name not specified. Usage: service.py <device-name>')
            sys.exit (-1)

        config_file = os.getenv('CE_SVC_CONFIG_PATH', 'config/config.yaml')
        ct.init_config(config_file)
        self.conf = ct.conf
        logging.debug('config:\n{}'.format(self.conf))
        if self.conf == {}:
            logging.error('Error in loading the config file')
            sys.exit (-1)

        self.device_name = sys.argv[1]
        if self.device_name is None:
            logging.error ('device_name not supplied')
            sys.exit (-1)