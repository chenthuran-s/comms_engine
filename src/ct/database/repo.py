import abc, logging

import psycopg2

ABC = abc.ABCMeta('ABC', (object,), {'__slots__': ()})

class RepoBase(ABC):
    def __init__(self, dsn):
        self.dsn = dsn
        if not self.connect_database():
            raise Exception ("fatal error: could not connect to database on server startup")


    def connect_database (self):         
        try:
            logging.info ('connecting to db with dsn =%s', self.dsn)
            dbh = psycopg2.connect (self.dsn)
            dbh.set_isolation_level (psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
            dbh.set_client_encoding ('UTF8')
            logging.info ('connected to db')
            self.dbh = dbh

            return True

        except Exception as ex:
            logging.exception ("failed to connect to database: " + str (ex))
            return False

    def is_connected(self):
        try:
            sth = self.dbh.cursor()
            sth.execute('select 1')
            return True
        except Exception as ex:
            logging.exception("database is not connected: " + str(ex))
            return False
