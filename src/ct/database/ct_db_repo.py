import logging

from ct.database.repo import RepoBase

class CartrackDbRepo(RepoBase):
    def __init__(self, dsn):
        RepoBase.__init__(self, dsn)

    def add(self, from_id, msg, msg_type, attachment_url=None, vehicle_case_id=0, in_reply_id=None):
        sql = "select * From cc.sp_process_socialmedia(%(from_id)s,now(),now(),%(msg)s,%(msg_type)s,%(in_reply_id)s,null,%(vehicle_case_id)s)"
        logging.info ("SQL to observer=%s" % sql)
        sth = self.dbh.cursor()
        query = sql
        logging.debug (query)
        logging.debug ("from_id: %s, msg: %s, msg_type: %s, vehicle_case_id: %s, reply_id: %s" % (from_id, msg, msg_type, vehicle_case_id, in_reply_id))
        sth.execute (query, { "from_id": from_id, "msg": msg, "msg_type": msg_type, "vehicle_case_id": vehicle_case_id, "in_reply_id": in_reply_id })

        if (sth.rowcount <= 0):
            sth.close() 
            logging.error ("zero data rows returned")
            return None
        
        result = sth.fetchone()[0]
        sth.close() 

        logging.debug ("updated messages up to id %s" % (result,))

        return result

    def update_messages (self, in_msisdn, in_text, msg_type):
        sql = "select * From cc.sp_process_socialmedia(%(in_msisdn)s,now(),now(),%(in_text)s,%(msg_type)s)"
        logging.info ("SQL to observer=%s" % sql)
        sth = self.dbh.cursor()
        query = sql
        logging.debug (query)
        logging.debug ("in_msisdn: %s, in_text: %s" % (in_msisdn, in_text))
        sth.execute (query, { "in_msisdn": in_msisdn, "in_text": in_text, "msg_type": msg_type })

        if (sth.rowcount <= 0):
            sth.close() 
            logging.error ("zero data rows returned")

            return None
        
        result = sth.fetchone()[0]
        sth.close() 

        logging.debug ("updated messages up to id %s" % (result,))

        return result

    def update_message (self, sm_id, in_text):
        sql = "update cc.\"cc$auto_in_socialmedia\" set payload=%(in_text)s where \"cc$auto_in_socialmedia_id\"=%(sm_id)s"
        logging.info ("SQL to observer=%s" % sql)
        sth = self.dbh.cursor()
        query = sql
        logging.debug (query)
        logging.debug ("sm_id: %s, in_text: %s" % (sm_id, in_text))
        sth.execute (query, { "sm_id": sm_id, "in_text": in_text })

        if (sth.rowcount <= 0):
            sth.close() 
            logging.error ("update failed")

            return False
        sth.close() 

        logging.debug ("updated messages up to id %s" % (sm_id,))

        return True

    def add_to_attachments (self, key_type, key_id, filename, obj_store_rel_path):
        sql = "insert into ct.attachment (key_type, key_id, filename, create_ts, object_store_relative_path) values (%(key_type)s, %(key_id)s, %(filename)s, now(), %(obj_store_rel_path)s)"
        sth = self.dbh.cursor()
        query = sql
        logging.debug (query)
        logging.debug ("key_type: %s\nkey_id: %s\nfilename: %s\npath: %s" % (key_type, key_id, filename, obj_store_rel_path))
        sth.execute (query, { "key_type": key_type, "key_id": key_id, "filename": filename, "obj_store_rel_path": obj_store_rel_path })

        if (sth.rowcount <= 0):
            sth.close() 
            logging.error ("insertion into ct.attachment failed")

            return False
        sth.close() 

        logging.debug ("inserted into ct.attachment")

        return True

    def get_client_id (self, msisdn):
        sql = "select out_client_id from cc.sp_get_client_from_contact(%(msisdn)s)"
        logging.debug(sql)
        logging.debug({ "msisdn": msisdn })
        sth = self.dbh.cursor()
        sth.execute(sql, { "msisdn": msisdn })

        if (sth.rowcount > 0):
            result = sth.fetchone()
            return result[0]
        else:
            return None

    def create_case(self, user_id, lat, lng):
        sql = "select * from ct.sp_controlroom_signal_alert_mobile_validate(0, %(user_id)s, now(), now(),'NOTIFY EMERGENCY CONTACT',6,%(in_lat)s,%(in_lon)s,2,665,1,2,11062,999,9999,true,20,'')"
        logging.debug(sql)
        query_params = { "user_id": user_id, "in_lat": lat, "in_lon": lng }
        logging.debug(query_params)

        sth = self.dbh.cursor()
        sth.execute(sql, query_params)

        if (sth.rowcount > 0):
            result = sth.fetchone()
            logging.info("sp result: %s", result)
            if result[0] == True:
                return True
            else:
                return False
        else:
            return False

    def get_json_message(self, msg_id):
        sql = "select * from cc.sp_process_to_json_messages(%(msg_id)s)"
        logging.debug(sql)
        query_params = { "msg_id": msg_id }
        logging.debug(query_params)

        sth = self.dbh.cursor()
        sth.execute(sql, query_params)

        if sth.rowcount > 0:
            result = sth.fetchone()
            logging.debug("sp result: %s" % result)
            return result
        else:
            logging.error("could not get json message for id %s" % msg_id)
            return None

    def update_msg_uuid(self, sm_id, uuid):
        sql = "update cc.\"cc$auto_in_socialmedia\" set \"uuid\"=%(uuid)s where sm_tx_queue_id=%(sm_id)s"
        logging.debug(sql)
        query_params = { "uuid": uuid, "sm_id": sm_id }
        logging.debug(query_params)

        sth = self.dbh.cursor()
        sth.execute(sql, query_params)

        if (sth.rowcount <= 0):
            sth.close() 
            logging.error ("update failed")

            return False
        sth.close() 

        logging.debug ("updated message with id %s" % (sm_id,))

        return True

    def update_msg_state(self, uuid, sm_state, failure_reason=None):
        sql = "update cc.\"cc$auto_in_socialmedia\" set sm_state=%(sm_state)s, failure_reason=%(failure_reason)s where \"uuid\"=%(uuid)s"
        logging.debug(sql)
        query_params = { "uuid": uuid, "sm_state": sm_state, "failure_reason": failure_reason }
        logging.debug(query_params)

        sth = self.dbh.cursor()
        sth.execute(sql, query_params)

        if (sth.rowcount <= 0):
            sth.close() 
            logging.error ("update failed")

            return False
        sth.close() 

        logging.debug ("updated message with uuid %s" % (uuid,))

        return True