import psycopg2
import logging
import time 
from random import randint

class OTPConfiguration:
    PIN_TIMEOUT = 3600 #in seconds


class OTPStatus:
    # OTPstatus may be stored as a 3 char code in database where required 
    EXCEPTION = "EXP"
    USER_UNKNOWN = "UUK"
    TARGET_UNKNOWN = "TUK"
    TARGET_TRUSTED = "TRU"
    OTP_REQUESTED = "OTP"
    USER_REGISTERED = "REG"
    USER_QUARANTINED = "UQT"
    OTP_WRONG = "OTW"
    OTP_EXPIRED = "OTE"


class OTPMixin:
    # send OTP using SMS to target device
    def send_sms (self, target, data):
        sql = 'select * from se.sm_submit(%s,%s::varchar::bytea,false,500,true)'
        args = (target, data)
        sth = self.dbh.cursor()
        logging.debug (sth.mogrify (sql, args))
        sth.execute (sql, args)

        if (sth.rowcount <= 0):
            sth.close() 
            logging.debug ("sm_submit failed on target %s" % target)

            return False
        
        sms = sth.fetchone()[0]
        sth.close() 
        logging.debug ("sm_submit succeeded on target %s sms transmission index = %s" % (target, sms))

        return True
        
    # returns a tuple (status, [target]) 
    # target will be None when user is UNKNOWN 
    def get_user_status (self, user):
        sql = 'select * from se.sm_user where user_id = %s and target like %s'
        args = (user, 'line:%')
        sth = self.dbh.cursor(cursor_factory=psycopg2.extras.DictCursor)
        logging.debug (sth.mogrify (sql, args))
        sth.execute (sql, args)

        if (sth.rowcount <= 0):        
            sth.close() 
            logging.debug ("zero matches found for %s in user db" % user)

            return (OTPStatus.USER_UNKNOWN, None)

        if (sth.rowcount == 1):        
            logging.debug ("match found for %s in user db" % user)
            data = sth.fetchone()
            otp = data['otp']
            if otp.isdigit():
                sth.close() 
                logging.debug ("user %s has requested an OTP %s " % (user, otp))

                return (OTPStatus.OTP_REQUESTED, data['target'])

            sth.close() 

            # User status is stored as 'OTPStatus' string in otp field of the database
            logging.debug ("user %s status is %s" % (user, otp))

            return (otp, data['target'])

        sth.close() 
        logging.debug ("exception: user_id is duplicated for user %s" % user)

        return (OTPStatus.EXCEPTION, None)

    # returns a tuple (status, [user]) 
    # user will be None when target is UNKNOWN *or* no user id has been linked to it
    def get_target_status (self, target):
        sql = 'select * from se.sm_user where target = %s'
        if (not target.startswith ('line:')):
            target = 'line:' + target
        args = (target,)
        sth = self.dbh.cursor(cursor_factory=psycopg2.extras.DictCursor)
        logging.debug (sth.mogrify (sql, args))
        sth.execute (sql, args)

        if (sth.rowcount <= 0):        
            sth.close() 
            logging.debug ("zero matches found for target %s in user db" % target)

            return (OTPStatus.TARGET_UNKNOWN, None)

        if (sth.rowcount == 1):        
            user = sth.fetchone()['user_id']
            sth.close() 
            logging.debug ("target %s trusted for user %s" % (target, user))

            return (OTPStatus.TARGET_TRUSTED, user)

        sth.close() 
        logging.debug ("exception: target %s is duplicated" % target)

        return (OTPStatus.EXCEPTION, None)

    def get_target_for_user (self, user):
        sql = 'select target from se.sm_user where user_id = %s and otp <> \'OTW\''
        args = (user,)
        sth = self.dbh.cursor(cursor_factory=psycopg2.extras.DictCursor)
        logging.debug (sth.mogrify (sql, args))
        sth.execute (sql, args)

        if (sth.rowcount <= 0):        
            sth.close() 
            logging.debug ("zero matches found for user %s in user db" % user)

            return (OTPStatus.TARGET_UNKNOWN, None)

        if (sth.rowcount == 1):        
            target = sth.fetchone()['target']
            sth.close() 
            logging.debug ("target %s trusted for user %s" % (target, user))

            return (OTPStatus.TARGET_TRUSTED, target)

        sth.close() 
        logging.debug ("exception: user %s is duplicated" % user)

        return (OTPStatus.EXCEPTION, None)


    def issue_otp (self, target, user):
        otp = self._generate_otp()
        otp_message = 'Your Cartrack OTP is %s please send it via your LINE messenger to complete registration' %  (otp)
        if (not self.send_sms (target, otp_message)):
            logging.debug ("sending OTP %s to target %s failed" % (otp, target))

            return False

        sql = 'update se.sm_user set otp=%s, ts=to_timestamp(%s), user_id=%s where target = %s'
        target = 'line:' + target
        epoch = time.time()
        args = (otp, epoch, user, target)
        sth = self.dbh.cursor()
        logging.debug (sth.mogrify (sql, args))
        sth.execute (sql, args)
        rows_affected = sth.rowcount

        if (rows_affected == 1):
            sth.close() 
            logging.debug ("updated user db for target %s " % target)

            return True
        
        elif (rows_affected < 0):        
            logging.debug ("updating user db failed for target %s " % target)

        elif (rows_affected > 1):
            logging.debug ("critical error: update user db failed target is duplicated for %s" % target)

        elif (rows_affected == 0):
            logging.debug ("critical error: update user db failed target not found for %s" % target)

        sth.close() 

        return False 

    def validate_otp (self, user, otp):
        sql = 'select otp, extract (epoch from ts) as epoch from se.sm_user where user_id = %s and target like %s'
        args = (user, 'line:%')
        sth = self.dbh.cursor(cursor_factory=psycopg2.extras.DictCursor)
        logging.debug (sth.mogrify (sql, args))
        sth.execute (sql, args)

        if (sth.rowcount == 1):        
            logging.debug ("OTP user %s found in db" % user)
            data = sth.fetchone()
            issued_time = data['epoch']
            issued_otp = data['otp']

            if (issued_otp == otp):
                sth.close()
                interval = time.time() - issued_time
                logging.debug ("time passed since issue of OTP is %d seconds" % interval)
                if (interval <= 0):
                    logging.debug ("exception: something is broken time since OTP issued cannot be %d" % interval)

                    return OTPStatus.EXCEPTION

                if (interval > OTPConfiguration.PIN_TIMEOUT):
                    logging.debug ("OTP expired time passed since issue is %d seconds" % interval)
                    self._delete_user (user)
    
                    return OTPStatus.OTP_EXPIRED

                if (self._update_user_status (user, OTPStatus.USER_REGISTERED)):
                    logging.debug ("OTP user %s is now registered" % user)

                    return OTPStatus.USER_REGISTERED

            sth.close() 
            logging.debug ("OTP user %s does not match" % user)
            self._delete_user (user)

            return OTPStatus.OTP_WRONG

        sth.close() 
        logging.debug ("error: OTP user %s not found in db" % user)

        return OTPStatus.USER_UNKNOWN

    def add_target (self, target):
        sql = 'insert into se.sm_user values (%s, %s, to_timestamp(%s), %s)'
        epoch = time.time()
        args = (target, None, epoch, None)
        sth = self.dbh.cursor(cursor_factory=psycopg2.extras.DictCursor)
        logging.debug (sth.mogrify (sql, args))
        sth.execute (sql, args)
        rows_affected = sth.rowcount

        if (rows_affected == 1):
            sth.close() 
            logging.debug ("inserted new target %s into user db" % target)

            return True
        
        elif (rows_affected <= 0):
            logging.debug ("critical error: inserting target %s into user db failed " % target)

        sth.close() 

        return False 

    #only users with trusted targets can be quarantined 
    def quarantine_user (self, user):
        return self._update_user_status (user, OTPStatus.USER_QUARANTINED)

    def _generate_otp (self):
        otp = ''.join (["%s" % randint (0, 9) for num in range (0, 6)])

        return otp

    def _delete_user (self, user):
        sql = 'delete from se.sm_user where user_id = %s'
        args = (user,)
        sth = self.dbh.cursor(cursor_factory=psycopg2.extras.DictCursor)
        logging.debug (sth.mogrify (sql, args))
        sth.execute (sql, args)
        rows_affected = sth.rowcount

        if (rows_affected >= 1):
            sth.close() 
            logging.debug ("removed user: %s" % user)

            return True
        
        elif (rows_affected < 0):        
            logging.debug ("failed to remove user %s" % user)

        elif (rows_affected == 0):
            logging.debug ("critical error: remove user %s failed user not found" % user)

        sth.close()

    def _update_user_status (self, user, otp):
        sql = 'update se.sm_user set otp=%s, ts=to_timestamp(%s) where user_id = %s and target like %s'
        epoch = time.time()
        args = (otp, epoch, user, 'line:%')
        sth = self.dbh.cursor(cursor_factory=psycopg2.extras.DictCursor)
        logging.debug (sth.mogrify (sql, args))
        sth.execute (sql, args)
        rows_affected = sth.rowcount

        if (rows_affected == 1):
            sth.close() 
            logging.debug ("updated user %s status" % user)

            return True
        
        elif (rows_affected < 0):        
            logging.debug ("updating user %s status failed" % user)

        elif (rows_affected > 1):
            logging.debug ("critical error: update user %s status failed user duplicated" % user)

        elif (rows_affected == 0):
            logging.debug ("critical error: update user %s status failed user not found" % user)

        sth.close() 

        return False
