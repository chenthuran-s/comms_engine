import logging
from yaml import load, Loader

def byteaToUnicode(d):
    # can be a buffer object; make it a string object
    if d is not None and d.__class__.__name__ != 'str':
        d = str(d)
    
    # convert data into unicode
    try:
        d = d.decode('utf-8')
    except UnicodeDecodeError:
        # failed to decode text from utf-8 to (internal) ucs2: error
        logging.info('failed to decode text from utf-8')
    
        try:
            d = d.decode('utf_16')
        except UnicodeDecodeError:
            # failed to decode text from utf-8 to (internal) ucs2: error
            logging.info('failed to decode text from utf-16; giving up')
            return None
    
    return d

def init_config(config_file):
    try:
        global conf
        f = open(config_file, 'r')
        # logging.info('contents:\n{}'.format(f.readlines()))
        conf = load(f, Loader=Loader)
        # logging.info('config:\n{}'.format(conf))
    except IOError:
        conf = {}
        logging.info('The config file does not exist: {}'.format(config_file))