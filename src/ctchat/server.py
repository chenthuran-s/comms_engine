import json, logging

from twisted.web import http
from twisted.web.resource import Resource

class CTChatServer (Resource):
    isLeaf = True

    def __init__(self, handler):
        Resource.__init__(self)
        self.handler = handler

    def render_GET (self, request):
        logging.debug ("Incoming GET request from %s" % request.getClientIP())
        logging.debug (request.args)

        request.setHeader("Content-Type", "application/json")
        request.setResponseCode(http.NOT_ALLOWED)
        return "".encode("utf-8")

    def render_POST (self, request):
        logging.debug ("Incoming POST request from %s" % request.getClientIP())

        str_body = request.content.read().decode("utf-8")
        logging.debug("request body: %s", str_body)
        data = json.loads(str_body)

        request.setHeader("Content-Type", "application/json")
        if self.handler.process_message(data):
            request.setResponseCode(http.OK)
        else:
            request.setResponseCode(http.BAD_REQUEST)

        return "".encode("utf-8")
