import json, logging

from twisted.web import http
from twisted.web.http_headers import Headers

from ct.web.request import HTTPRequest

class CTChatRequest (HTTPRequest):
    def __init__(self, handler, sm_id, sm_msg_time, sm_target, sm_data):
        self.handler = handler
        self.sm_target = sm_target
        self.sm_data = sm_data
        self.sm_id = sm_id
        self.time = sm_msg_time
        self.response_code = None
        self.error_msg = None
        self.url = handler.comms_.device.conf.get('submit_url', '')
        self.token = handler.comms_.device.conf.get('token', 'missing token')

        HTTPRequest.__init__(self, CTChatRequest._request_ok_2, CTChatRequest._request_failed)
    
    def get_method(self):
        return "POST".encode("utf-8")
    
    def get_headers(self):
        return Headers ({'User-Agent': ['Cartrack web client'], 'Content-Type': ['application/json'], 'authorization': ["Bearer " + self.token]})
    
    def get_url(self):
        return self.url.encode("utf-8")

    def _request_ok (self, response_code):
        logging.debug ("HTTP response code %d to %s" % (response_code, self.sm_target))
        self.response_code = response_code

    def _request_ok_2 (self):
        logging.debug ("HTTP response ok to %s" % (self.sm_target))
    
    def _request_failed (self):
        logging.error ("HTTP request failed for %s with error %s" % (self.sm_target, self.error_msg))

    def parse_response (self, resp):
        logging.debug ("Response: %s" % resp)
        if (self.response_code == http.OK): 
            return
        content = json.loads(resp)
        if content.get ("message"):
            self.error_msg = content["message"]
        else:
            self.error_msg = "No error message available"
 
        self._request_failed()

    def get_request_body (self):
        # data = {
        #     'clientId': self.sm_target.replace('ctchat:', ''),
        #     'message': self.sm_data,
        #     'messageId': self.sm_id,
        #     'time': self.time
        # }
        return json.dumps (self.sm_data).encode("utf-8")
