import logging

from twisted.internet import reactor

import ct
from ct.commsengine.client import Client
from ct.service import ServiceBase
from ctchat.handler import CTChatHandler

class CTChatClient (Client):
    def __init__(self, device_name, dsn):
        Client.__init__(self, device_name, dsn)

def main():
    logging.basicConfig (format='%(levelname)s:%(asctime)s:%(message)s', datefmt='%m/%d/%Y %H:%M:%S', level=logging.DEBUG)
    logging.info("Starting the Cartrack Chat Service")

    svc = ServiceBase()
    logging.info(svc.__dict__)

    try:
        comms_client = CTChatClient(svc.device_name, "dbname=%s host=%s user=%s password=%s" % (svc.conf['db_name'], svc.conf['db_host'], svc.conf['db_user'], svc.conf['db_pass']))
        h = CTChatHandler(comms_client)
        reactor.callWhenRunning(h.start)
        logging.info("starting reactor")
        reactor.run()
        h.stop_pg_listen = True
        logging.info("over and out")
    except Exception:
        logging.exception('error')
