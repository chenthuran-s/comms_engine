import datetime, json, logging, signal
from collections import deque

from pgnotify import await_pg_notifications, get_dbapi_connection
import psycopg2
import redis
from twisted.internet import reactor, threads
from twisted.web.server import Site

import ct
from ct.database.ct_db_repo import CartrackDbRepo
from ctchat.request import CTChatRequest
from ctchat.server import CTChatServer

class CTChatHandler:

    def __init__(self, comms_client):
        self.comms_ = comms_client
        self.comms_.set_on_new_sm (self.on_new_sm_fake)
        self.port_ = int (self.comms_.device.conf.get('listen_port', '8090'))
        self.msg_queue = deque() 
        self.synchronise_interval = 30
        self.ct_db = None
        self.redis_server = redis.Redis(ct.conf['redis_host'])
        self.stop_pg_listen = False

    def start(self):
        try:
            ct_dsn = "dbname=%s host=%s user=%s password=%s" % (ct.conf['ct_db_name'], ct.conf['ct_db_host'], ct.conf['ct_db_user'], ct.conf['ct_db_pass'])
            self.ct_db = CartrackDbRepo(ct_dsn)

            reactor.listenTCP(self.port_, Site(CTChatServer(self)))
            reactor.addSystemEventTrigger('before', 'shutdown', self.pg_disconnect)
            threads.deferToThread(self.listen_pg)
            logging.info("Listening for messages...")
        except Exception as ex:
            logging.exception(ex)
            reactor.stop()
            return

        # commenting out the poll
        self.comms_.start()

    def on_new_sm_fake(self, smRow):
        pass

    def on_new_sm (self, strData):
        
        data = json.loads(strData)
        logging.debug("Scanning all available web socket servers")
        msg_sent = False
        msisdn = data['out_msisdn']
        logging.debug ("New request: sm_id: %u, target: %s" % (data['out_message_id'], msisdn))
        for key in self.redis_server.scan_iter("*"):
            logging.debug("key: %s", key)
            for val in self.redis_server.lrange(key, 0, -1):
                logging.debug("value: %s (%s) ", val, msisdn)
                if val.decode("utf-8").startswith(msisdn):
                    logging.debug("send message to %s", key)
                    logging.debug("content: %s", data)
                    CTChatRequest(self, 0, datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"), msisdn, data).start()
                    msg_sent = True
                    break
                        

        if msg_sent == False:
            try:
                logging.info("sending push notification")
                fleet_dsn = "dbname=%s host=%s user=%s password=%s" % (ct.conf['fleet_db_name'], ct.conf['fleet_db_host'], ct.conf['fleet_db_user'], ct.conf['fleet_db_pass'])
                db_conn = psycopg2.connect (fleet_dsn)
                db_conn.set_isolation_level (psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
                db_conn.set_client_encoding ('UTF8')
                logging.debug ('connected to fleet db')

                cur = db_conn.cursor()

                sql = """select al.token_id, 
                            case 
                                when al.os_version ilike 'android%(hms)%' then 'hms'
                                when al.os_version ilike 'android%' then 'android'
                                when al.os_version ilike 'ios%' then 'ios'
                            end os
                        from safe.active_login al
                        join ct.client c on al.user_id = c.user_id
                        where c.mobile_number='""" + msisdn + "';"
                
                logging.debug(sql)
                cur.execute(sql)
                row = cur.fetchone()

                if row != None and row[0] != None and row[0] != "":
                    sql = "SELECT * FROM safe.send_push_notification(%(token_id)s, %(os)s,'New Message(s) Received',%(msg)s,'chat');"

                    logging.debug(sql)
                    logging.debug({ "token_id": row[0], "os": row[1], "msg": data['out_message_text'] })
                    cur.execute(sql, { "token_id": row[0], "os": row[1], "msg": data['out_message_text'] })
                    result = cur.fetchone()
                    if str(result[0]) == "1":
                        logging.info("push notification sent to %s using token %s" % msisdn, row[0])
                        msg_sent = True
                    else:
                        logging.warn(result[0])
                        msg_sent = False
                else:
                    logging.warn("no active loggin found")
                    msg_sent = False
            except Exception as ex:
                logging.exception ("failed to send push notification: " + str (ex))
            finally:
                if db_conn is not None:
                    db_conn.close()

        # if msg_sent:
        #     self.comms_.sm_sent (smRow['id'], None)

    def process_message(self, data):
        user_id = data.get("msisdn")
        msg = data.get("message")

        if user_id and msg:
            self.ct_db.add(user_id, msg['in_message_text'], "ctchat", vehicle_case_id=msg['in_vehicle_case_id'], in_reply_id=msg['in_reply_message_id'])
            return True
        else:
            logging.info("The msisdn / message was empty. The request was not processed")
            return False

    def listen_pg(self):
        conn_str = "postgresql://%s:%s@%s/cartrack" % (ct.conf['db_user'], ct.conf['db_pass'], ct.conf['db_host'])
        logging.debug("pg conn str: %s" % conn_str)

        e = get_dbapi_connection(conn_str)
        # SIGNALS_TO_HANDLE = [signal.SIGINT, signal.SIGTERM]

        for n in await_pg_notifications(
            e,
            ["sm_to_client_ctchat"],
            timeout=10,
            yield_on_timeout=True,
            # handle_signals=SIGNALS_TO_HANDLE,
        ):
            logging.debug("stop listening: %s" % self.stop_pg_listen)
            if self.stop_pg_listen:
                logging.info("stopping pg listener")
                break

            if isinstance(n, int):
                logging.info("stopping pg listener")
                break
            elif n is None:
                logging.debug("listening for 'sm_to_client_ctchat' messages")
            else:
                try:
                    logging.info("pg message recd: %s" % n.payload)
                    self.on_new_sm(n.payload)
                except Exception as ex:
                    logging.error(ex)

    def pg_disconnect(self):
        logging.debug("disconnect from pg")
        self.stop_pg_listen = True