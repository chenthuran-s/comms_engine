# -*- coding: utf-8 -*-
import logging
import time
import json
import urllib

import base64
import hashlib
import hmac

from twisted.web.server import Site
from twisted.web.static import File
from twisted.web.resource import Resource
from twisted.web import http
from twisted.internet import reactor

from ct.authentication.registration import OTPStatus

# Line server handles incoming LINE protocol webhooks 
#
# render_POST->process_post()->process_events()->process_message()
# process_message() builds context object
#
# process_post()->process_context()
# process_context() does all the actual work making DB calls and whatever actions required

class MessageTemplateKeys:
    OTP_EXPIRED = "otp_expired" 
    OTP_INVALID = "otp_invalid"
    OTP_RESEND = "otp_resend"
    OTP_SENT = "otp_sent"
    OTP_WRONG = "otp_wrong"
    REG_SUCCESS = "reg_success"
    UNKNOWN_USER = "unknown_user"

class LineServer (Resource):
    isLeaf = True
    
    def __init__(self, server_):
        Resource.__init__(self)
        self.server_ = server_
        self.context = None


    def render_GET (self, request):
        logging.debug ("Incoming GET request from %s" % request.getClientIP())
        logging.debug (request.args)

        request.setResponseCode (http.NOT_ALLOWED)
        return "Not Allowed"
    
    
    def render_POST (self, request):
        self.request = request
        logging.debug ("Incoming POST request from %s" % self.request.getClientIP())
      
        self.context = {}
        signature_header = request.getHeader ('X-Line-Signature'); 
        data = self.request.content.read().decode ('utf-8')
        if not self._validate_signature (data, signature_header):
            logging.debug ("Request signature can not be validated")

            self.request.setResponseCode (http.UNAUTHORIZED)
            return "Unauthorized" 
 
        logging.debug ("Incoming request insides: %s" % data)
        self._process_post (data)
     
        # actual return status is the response code set on the request
        # "Done" does not imply *no error* it just means we are done
        return "Done"
    
    
    def _process_post (self, data):
        try:
            arguments = json.loads (data)
            if arguments.get ('events') is None:
                logging.debug ("Request missing events field")

                self.request.setResponseCode (http.BAD_REQUEST)
                return            
 
            for event in arguments['events']:
                logging.debug ("event = %s" % str (event))
                response = self._process_event (event)

                if (response != http.OK):
                    self.request.setResponseCode (response)
                    return 
    
            return self._process_context() 

        except Exception as ex:
            logging.exception (ex) 
    
            self.request.setResponseCode (http.INTERNAL_SERVER_ERROR)


    def _process_event (self, event):
        if event.get ('type') is None:
            logging.debug ("Request missing events field")

            return http.BAD_REQUEST

        if event.get ('timestamp') is None:
            logging.debug ("Request missing timestamp field")

            return http.BAD_REQUEST
          
        if event.get ('source') is None:
            logging.debug ("Request missing source field")

            return http.BAD_REQUEST

        self.context['userId'] = event['source']['userId']
        self.context['timestamp'] = event['timestamp']
        self.context['replyToken'] = event['replyToken']

        # These are webhook verification messages sent by the LINE platform  
        if (self.context['replyToken'] in ["00000000000000000000000000000000", "ffffffffffffffffffffffffffffffff"]):

            return http.OK 

        event_type = event['type']
        if (event_type == 'message'):
            return self._process_message (event['message'])
 
        logging.debug ("No processor for event type %s" % event_type)

        return http.BAD_REQUEST

    def _process_message (self, message):
        msg_type = message.get ('type')
        if msg_type is not None:
            if msg_type == 'text':
                self.context['type'] = message['type']
                self.context['text'] = message['text']
                self.context['msg_id'] = message['id']
                self._analyse_text()
             
                return http.OK

            if msg_type in ['image', 'audio', 'video', 'file' ]:
                self.context['type'] = message['type']
                self.context['msg_id'] = message['id']
                self.context['content_provider'] = message.get("contentProvider", {}).get("type")

                return http.OK

            if msg_type in ['location']:
                self.context['type'] = message['type']
                self.context['msg_id'] = message['id']
                self.context['latitude'] = message['latitude']
                self.context['longitude'] = message['longitude']
                self.context['address'] = message['address']

                return http.OK

            
        logging.debug ("No analyser for message type: %s" % message.get ('type') )

        return http.BAD_REQUEST


    def _analyse_text (self):
        text = self.context['text']

        if text.startswith ("+"): 
            text = text[1:]

        # see https://github.com/googlei18n/libphonenumber/blob/master/FAQ.md for guidelines
        target_status, target = self.server_.on_get_target_for_user(self.context['userId'])
        if (text.isdigit()):
            l = len (text)
            if ((l > 2) and (l < 17) and target_status == OTPStatus.TARGET_UNKNOWN):
                logging.debug ("looks like a mobile number %s" % text)
                self.context['target'] = text
                
                return
            else:
                logging.debug("looks like an otp attempt from %s with %s" % (target, text))
                self.context['target'] = target
                self.context['otp'] = text

        # if text.startswith ("P"): 
        #     text = text[1:]
        #     if (text.isdigit() and (len (text) == 6)):
        #         logging.debug ("looks like an OTP %s" % text)
        #         self.context['otp'] = text
                
        #         return

    def get_message_text(self, key):
        message_templates = self.server_.comms_.device.conf.get ('message_templates')
        if message_templates != None:
            message_text = message_templates.get(key)
            if message_text != None and len(message_text.strip()) > 0:
                return message_text
            else:
                logging.debug("No message template found for '%s'", key)
        else:
            logging.debug("Message templates have not been setup for the driver")
            
        return None


    def _process_context (self):
        user_status, user_target = self.server_.on_get_user_status (self.context['userId'])

        if (user_status in [OTPStatus.USER_UNKNOWN, OTPStatus.OTP_EXPIRED, OTPStatus.OTP_WRONG]):
            logging.debug ("user is not registered - userId = %s, status = %s" % (self.context['userId'], user_status))
            target = self.context.get ('target')
            if (target is not None):
                target_status, target_user = self.server_.on_get_target_status (target)
                if (target_status == OTPStatus.TARGET_TRUSTED):
                    if (self.server_.on_issue_otp (target, self.context['userId'])):
                        logging.debug ("user issued with OTP - userId = %s" % self.context['userId'])
                        # self.server_.on_line_msg (self.context['userId'], user_target, u"OTP sent. Please check your SMS and reply to this message with your OTP.\n\nส่งรหัส OTP เรียบร้อยแล้ว โปรดตรวจสอบใน SMS ของคุณ และพิมพ์รหัส OTP ที่คุณได้รับ")
                        self.server_.on_line_msg (self.context['userId'], user_target, self.get_message_text(MessageTemplateKeys.OTP_SENT))
                        
                        return http.OK
                    else:
                        logging.debug ("error could not issue OTP - userId = %s" % self.context['userId'])
                
                        return http.UNAUTHORIZED
                else:
                    # logging.debug ("error target not trusted could not issue OTP - userId = %s" % self.context['userId'])
                    logging.debug ("New user detected - userId = %s" % self.context['userId'])
                    logging.debug ("Adding target %s" % "line:" + target)
                    if self.server_.on_add_target("line:" + target):
                        target_status, target_user = self.server_.on_get_target_status (target)
                        if (target_status == OTPStatus.TARGET_TRUSTED):
                            if (self.server_.on_issue_otp (target, self.context['userId'])):
                                logging.debug ("user issued with OTP - userId = %s" % self.context['userId'])
                                # self.server_.on_line_msg (self.context['userId'], user_target, u"OTP sent. Please check your SMS and reply to this message with your OTP.\n\nส่งรหัส OTP เรียบร้อยแล้ว โปรดตรวจสอบใน SMS ของคุณ และพิมพ์รหัส OTP ที่คุณได้รับ")
                                self.server_.on_line_msg (self.context['userId'], user_target, self.get_message_text(MessageTemplateKeys.OTP_SENT))

                                return http.OK
                            else:
                                logging.debug ("error could not issue OTP - userId = %s" % self.context['userId'])
                        
                                return http.UNAUTHORIZED
                        else:
                            logging.debug ("critical error could not find target that was added - target = %s" % target)
                    else:
                        return http.UNAUTHORIZED

            if (user_status != OTPStatus.USER_UNKNOWN): 
                # self.server_.on_line_msg (self.context['userId'], user_target, u"Unfortunately you are not registered. Please send your mobile number first so that Cartrack may confirm your identity.\n\nน่าเสียดายที่คุณยังไม่ได้ลงทะเบียน กรุณาพิมพ์หมายเลขโทรศัพท์มือถือของคุณ เพื่อให้ทาง Cartrack สามารถยืนยันตัวตนของคุณ")
                self.server_.on_line_msg (self.context['userId'], user_target, self.get_message_text(MessageTemplateKeys.UNKNOWN_USER))

                # target must be trusted if an OTP was issued before
                self.server_.on_quarantine_user (self.context['userId'])
                logging.debug ("user quarantined - userId = %s" % self.context['userId'])
            else:
                logging.debug ("user unknown - userId = %s" % self.context['userId'])
                # self.server_.on_line_msg (self.context['userId'], user_target, u"Sorry, we are unable to recognize you. To complete your registration, please reply to this message with your phone number (international format: 66871553331) and enter the OTP that we will SMS you")
                self.server_.on_line_msg (self.context['userId'], user_target, self.get_message_text(MessageTemplateKeys.UNKNOWN_USER))

            return http.UNAUTHORIZED

        elif (user_status == OTPStatus.OTP_REQUESTED):
            otp = self.context.get ('otp')
            if (otp is not None):
                otp_status = self.server_.on_validate_otp (self.context['userId'], otp)
                if (otp_status == OTPStatus.USER_REGISTERED):
                    logging.debug ("user registration successful - userId = %s" % self.context['userId'])
                    # self.server_.on_line_msg (self.context['userId'], user_target, u"Congratulations, registration complete! You can now contact us on Line whenever you need.\n\nยินดีด้วย การลงทะเบียนเสร็จสิ้นแล้ว! คุณสามารถติดต่อกับเราทาง LINE ได้ทุกเมื่อที่คุณต้องการ")
                    self.server_.on_line_msg (self.context['userId'], user_target, self.get_message_text(MessageTemplateKeys.REG_SUCCESS))
 
                    return http.OK

                elif (otp_status == OTPStatus.OTP_EXPIRED):
                    logging.debug ("OTP expired - userId = %s" % self.context['userId'])
                    # self.server_.on_line_msg (self.context['userId'], user_target, u"I'm afraid your OTP expired. Please send your mobile number again so we can issue you with a new OTP.\n\nรหัส OTP ของคุณไม่ถูกต้อง กรุณาพิมพ์หมายเลขโทรศัพท์มือถือของคุณอีกครั้ง เพื่อให้เราส่งรหัส OTP ใหม่ให้คุณ")
                    self.server_.on_line_msg (self.context['userId'], user_target, self.get_message_text(MessageTemplateKeys.OTP_EXPIRED))
 
                    return http.UNAUTHORIZED

                elif (otp_status == OTPStatus.OTP_WRONG):
                    logging.debug ("OTP wrong - userId = %s" % self.context['userId'])
                    # self.server_.on_line_msg (self.context['userId'], user_target, u"I'm afraid your OTP is wrong. Please send your mobile number again so we can issue you with a new OTP.\n\nรหัส OTP ของคุณไม่ถูกต้อง กรุณาพิมพ์หมายเลขโทรศัพท์มือถือของคุณอีกครั้ง เพื่อให้เราส่งรหัส OTP ใหม่ให้คุณ")
                    self.server_.on_line_msg (self.context['userId'], user_target, self.get_message_text(MessageTemplateKeys.OTP_WRONG))
 
                    return http.UNAUTHORIZED

            target = self.context.get ('target')
            if (target is not None):
                    logging.debug ("Assuming OTP was not received - userId = %s" % self.context['userId'])

                    if (self.server_.on_issue_otp (target, self.context['userId'])):
                        logging.debug ("user issued with a new OTP - userId = %s" % self.context['userId'])
                        # self.server_.on_line_msg (self.context['userId'], target, u"OTP re-issued please check your SMS.\n\nส่งรหัส OTP ใหม่ให้คุณแล้ว โปรดตรวจสอบ SMS ของคุณ")
                        self.server_.on_line_msg (self.context['userId'], user_target, self.get_message_text(MessageTemplateKeys.OTP_RESEND))

                        return http.OK
                    else:
                        logging.debug ("error could not re-issue OTP - userId = %s" % self.context['userId'])
                
                        return http.UNAUTHORIZED
            else:
                logging.debug ("Incorrect OTP - userId = %s" % self.context['userId'])
                # self.server_.on_line_msg (self.context['userId'], target, u"The OTP is invalid. Please provide a valid OTP to complete the registration")
                self.server_.on_line_msg (self.context['userId'], user_target, self.get_message_text(MessageTemplateKeys.OTP_INVALID))



            return http.UNAUTHORIZED
        
        elif (user_status == OTPStatus.USER_REGISTERED):
            logging.debug ("received user message - userId = %s" % self.context['userId'])
            if self.context['type'] == 'text':
                text = self.context['text']
                self.server_.on_sms_deliver (user_target, text.encode('utf-8'))
                self.context['target'] = user_target
                self.server_.queue_for_processing (self.context)
                return http.OK

            if self.context['type'] in ['image', 'audio', 'video', 'file', 'location' ]:
                self.context['target'] = user_target
                self.server_.queue_for_processing (self.context)
                return http.OK
            
            return http.BAD_REQUEST
            
        
        elif (user_status == OTPStatus.USER_QUARANTINED):
            logging.debug ("received quarantined user message - userId = %s" % self.context['userId'])
            target = self.context.get ('target')
            if (target is not None):
                target_status, target_user = self.server_.on_get_target_status (target)
                if (target_status == OTPStatus.TARGET_TRUSTED):
                    if (self.server_.on_issue_otp (target, self.context['userId'])):
                        logging.debug ("user issued with OTP - userId = %s" % self.context['userId'])
                        # self.server_.on_line_msg (self.context['userId'], user_target, u"OTP sent. Please check your SMS and reply to this message with your OTP.\n\nส่งรหัส OTP เรียบร้อยแล้ว โปรดตรวจสอบใน SMS ของคุณ และพิมพ์รหัส OTP ที่คุณได้รับ")
                        self.server_.on_line_msg (self.context['userId'], user_target, self.get_message_text(MessageTemplateKeys.OTP_SENT))

                        return http.OK
                    else:
                        logging.debug ("error could not issue OTP - userId = %s" % self.context['userId'])
                else:
                    logging.debug ("error target not trusted could not issue OTP - userId = %s" % self.context['userId'])
            
            logging.debug ("quarantined user message dropped - userId = %s" % self.context['userId'])

            return http.UNAUTHORIZED

        elif (user_status == OTPStatus.EXCEPTION):
            logging.debug ("user status exception occured - userid = %s" % self.context['userId'])

            return http.BAD_REQUEST

        logging.debug ("user status '%s' unknown - userId = %s" % (user_status, self.context['userId']))

        return http.BAD_REQUEST


    def _validate_signature (self, request_content, signature_header):
            channel_secret = self.server_.comms_.device.conf.get ('password')
            my_hash = hmac.new (channel_secret.encode ('utf-8'), request_content.encode ('utf-8'), hashlib.sha256).digest()
            signature = base64.b64encode (my_hash)
            logging.debug("%s : %s" % (signature_header, signature.decode('utf-8')))
            return (signature_header == signature.decode('utf-8')) 
