import logging
from base64 import b64encode

from twisted.web import http
from twisted.web.http_headers import Headers

from ct.web.request import HTTPRequest
import json

class LineRequest (HTTPRequest):
    def __init__(self, url, token, on_sent, on_fail):
        HTTPRequest.__init__(self, on_sent, on_fail)
        self.url = url
        self.token = token 
    
    def get_method(self):
        return "POST".encode("utf-8")
    
    
    def get_headers(self):
        authorization = "{%s}" % (self.token)
        return Headers ({'User-Agent': ['Cartrack web client'], 'Content-Type': ['application/json'], 'authorization': ["Bearer " + authorization]})
    
    def get_url(self):
        return self.url.encode("utf-8")

class LineSMSRequest (LineRequest):
    def __init__(self, server, user, sm_id, sm_target, sm_data):
        self.server = server
        self.sm_id = None
        self.sm_target = sm_target
        self.sm_data = sm_data
        self.user = user
        self.response_code = None
        self.error_msg = None
        url = server.comms_.device.conf.get('submit_url', '')
        token = server.comms_.device.conf.get('token')
        LineRequest.__init__(self, url, token, self._request_ok, self._request_failed)
    
    def _request_ok (self, response_code):
        logging.debug ("HTTP response code %d for user %s to %s" % (response_code, self.user, self.sm_target))
        self.response_code = response_code
    
    def _request_failed (self):
        logging.debug ("HTTP request failed for user %s to %s with error %s" % (self.user, self.sm_target, self.error_msg))
        if (self.sm_id is not None):
            self.server.on_sms_request_failed (self.sm_id, None, 'HTTP request failed: ' + self.error_msg)

    def parse_response (self, resp):
        logging.debug ("Response: %s" % resp)
        if (self.response_code == http.OK): 
            if (self.sm_id is not None):
                self.server.on_sms_request_sent (self.sm_id, self.user)
            return
        content = json.loads(resp)
        if content.get ("message"):
            self.error_msg = content["message"]
        else:
            self.error_msg = "No error message available"
 
        self._request_failed()

    def get_request_body (self):
        msg = bytearray (self.sm_data,'utf-8').decode ('utf-8')
        data = {'to': self.user,'messages': [{'type': 'text','text': msg}]}

        return json.dumps (data).encode("utf-8") 

