import logging
import time
import datetime
import mimetypes
import psycopg2
import requests

from twisted.web.server import Site
from twisted.web.static import File
from twisted.web.resource import Resource
from twisted.internet import ssl, reactor

from line.database_connector import DatabaseConnector
from line.request import LineSMSRequest
from line.server import LineServer

import ct
from ct.encryption.webserver import ChainedOpenSSLContextFactory
from ct.authentication.registration import OTPStatus

import os
from collections import deque



class LineHandler:

    def __init__(self, comms_client, attachment_service):
        self.comms_ = comms_client
        self.attachment_service = attachment_service
        self.comms_.set_on_new_sm (self._on_new_sm)
        self.port_ = int (self.comms_.device.conf.get('listen_port', '81'))
        self.msg_queue = deque() 
        self.synchronise_interval = 30

    def start (self):
        try:
            ct_dsn = "dbname=%s host=%s user=%s password=%s" % (ct.conf['ct_db_name'], ct.conf['ct_db_host'], ct.conf['ct_db_user'], ct.conf['ct_db_pass'])
            self.ct_db = DatabaseConnector (ct_dsn) 

            reactor.listenTCP (self.port_, Site (LineServer(self)))
            # key_file = os.getenv('CE_SSL_KEY_PATH', '')
            # chain_crt_file = os.getenv('CE_SSL_CHAIN_CRT_PATH', '')
            # logging.debug("key file: %s" % key_file)
            # logging.debug("chain cert file: %s" % chain_crt_file)
            # reactor.listenSSL (self.port_, Site (LineServer(self)), ChainedOpenSSLContextFactory (
            # key_file, chain_crt_file))

        except Exception as ex:
            logging.exception (ex)
            reactor.stop()

            return

        self.comms_.start()
    
    def _on_new_sm (self, smRow):
        logging.debug ("New request: sm_id: %u, target: %s" % (smRow['id'], smRow['target']))
        status, user = self.comms_.get_target_status (smRow['target'])

        if (status == OTPStatus.TARGET_UNKNOWN):
            logging.debug ("Target %s is unknown adding it to the user db" % smRow['target'])
            if (not self.comms_.add_target (smRow['target'])):
                error_msg = "critical error could not add new targer %s to user db" % smRow['target']
                logging.debug ("Sending request failed for sm_id %d : %s" % (smRow['id'], error_msg))
                self.on_sms_request_failed (smRow['id'], None, error_msg)
            
                return

        if (user is not None):                             
            status, target = self.comms_.get_user_status (user)
            if (status == OTPStatus.USER_REGISTERED): 
                LineSMSRequest (self, user, smRow['id'], smRow['target'], ct.byteaToUnicode (smRow['data'])).start()
                self.on_sms_request_sent(smRow['id'], None)
                return

            error_msg = "user %s registration status not valid: status %s" % (user, status) 
            logging.debug ("Sending request failed for sm_id %d : %s" % (smRow['id'], error_msg))
            self.on_sms_request_failed (smRow['id'], None, error_msg)

            return

        error_msg = "target %s is not paired with a LINE user" % smRow['target']
        logging.debug ("Sending request failed for sm_id %d : %s" % (smRow['id'], error_msg))
        self.on_sms_request_failed (smRow['id'], None, error_msg)
    
    def on_sms_request_failed (self, sm_id, dev_sm_id, reason):
        #logging.info ("SMS request id: %d failed" % (sm_id))
        self.comms_.sm_failed (sm_id, dev_sm_id, reason)
    
    def on_sms_request_sent (self, sm_id, dev_sm_id):
        self.comms_.sm_sent (sm_id, dev_sm_id)
    
    def on_sms_request_delivered (self, sm_id, dev_sm_id):
        self.comms_.sm_delivered (sm_id, dev_sm_id)
    
    def on_sms_deliver (self, sender, data):
        self.comms_.sm_deliver (data, sender, None)

    def on_get_user_status (self, user):
        return self.comms_.get_user_status (user) 
 
    def on_get_target_status (self, target):
        return self.comms_.get_target_status (target) 

    def on_issue_otp (self, target, user):
        return self.comms_.issue_otp (target, user)

    def on_validate_otp (self, user, otp):
        return self.comms_.validate_otp (user, otp)

    def on_quarantine_user (self, user):
        return self.comms_.quarantine_user (user) 
  
    def on_add_target (self, target):
        return self.comms_.add_target (target) 

    def on_line_msg (self, user, target, msg):
        logging.debug ("Sending line message '%s' to userId %s" % (msg, user))
        LineSMSRequest (self, user, None, target, msg).start()

    def on_get_target_for_user(self, user):
        return self.comms_.get_target_for_user(user)

    def queue_for_processing (self, data):
        data_type = data.get('type', '')
        if (data_type == 'text'):
            # self.msg_queue.append (data)
            logging.debug ("Processing inbound line text message")
            number = str (data['target'][5:])
            text = data['text'].encode ('utf-8')
            timestamp = datetime.datetime.utcfromtimestamp (int (data['timestamp'])/1000).strftime ('%Y-%m-%d %H:%M:%S')
            message_uuid = str (data['msg_id'])

            self.ct_db.update_messages (number,text)
            
            return 

        if data_type in ['image', 'audio', 'video', 'file' ]:
            if data['content_provider'] == 'line':
                logging.debug ("Processing inbound line attachment message")
                url_format = self.comms_.device.conf.get("content_url_format")
                token = self.comms_.device.conf.get("token")
                if url_format is not None:
                    url = url_format.format(data['msg_id'])
                    logging.debug("downloading content from url: %s" % url)
                    headers = {
                        'Authorization': 'Bearer ' + token
                    }
                    response = requests.get(url, headers=headers)
                    if response.status_code == 200:
                        logging.debug("%s bytes downloaded", str(len(response.content)))
                        number = str (data['target'][5:])
                        sm_id = self.ct_db.update_messages(number, 'File received, uploading to the server now...')
                        res_content_type = response.headers['Content-Type']
                        logging.debug("content_type: %s, message_type: %s" % (res_content_type, data_type))
                        file_ext = mimetypes.guess_extension(res_content_type)
                        if file_ext == None:
                            if data_type == 'audio':
                                file_ext = ".mp3"
                            elif data_type == 'video':
                                file_ext = ".mp4"
                            elif data_type == 'image':
                                file_ext = ".jpeg"
                            else:
                                file_ext = ""

                        logging.debug("file_ext: %s" % file_ext)
                        filename = "{}{}".format(data['msg_id'], file_ext)
                        result = self.attachment_service.upload(sm_id, response.headers['Content-Type'], response.content, filename)
                        if result is not None:
                            try:
                                self.ct_db.add_to_attachments('social_media', sm_id, filename, result)
                            except Exception as ex:
                                logging.debug("failed to update ct.attachment table")
                                logging.debug(ex)
                                
                            self.ct_db.update_message(sm_id, '{}\nDouble click here to view the file'.format(result))
                        else:
                            self.ct_db.update_message(sm_id, "File upload failed")
                    else:
                        logging.debug("Could not download content")
                else:
                    logging.debug("content_url_format not found in the line driver config")
                return
            else:
                logging.debug("Content provider not supported")

        if data_type == 'location':
            logging.debug ("Processing inbound line location message")
            number = str (data['target'][5:])
            # text = "https://www.google.com/maps/@" + str(data.get('latitude', 0)) + "," + str(data.get('longitude', 0)) + ",20z\nThe client has shared their location"
            text = "https://maps.google.com/?q=" + str(data.get('latitude', 0)) + "," + str(data.get('longitude', 0)) + "\nThe client has shared a location"

            self.ct_db.update_messages (number,text)
            return

    # Not used any more
    # Messages are processed as and when they are received
    # def synchronise_cartrack_database (self):
    #     def synchronise_messages():
    #         values = []
    #         while self.msg_queue:
    #         	logging.debug ("Processing inbound line Messages")
    #             data = self.msg_queue.popleft()
    #             number = str (data['target'][5:])
    #             text = data['text'].encode ('utf-8')
    #             timestamp = datetime.datetime.utcfromtimestamp (int (data['timestamp'])/1000).strftime ('%Y-%m-%d %H:%M:%S')
    #             message_uuid = str (data['msg_id'])

    #             #values.append ((number, timestamp , timestamp, text))
    #             self.ct_db.update_messages (number,text)

    #         #if (values):
                 

    #     synchronise_messages()

    #     reactor.callLater (self.synchronise_interval, self.synchronise_cartrack_database)
