import psycopg2, logging
from twisted.python.failure import Failure

class DatabaseConnector:
    def __init__(self, dsn):
        self.dsn = dsn
        if not self.connect_database():

            raise Exception ("fatal error: could not connect to database on server startup")

    def update_messages (self, in_msisdn, in_text):
        sql = "select * From cc.sp_process_socialmedia(%(in_msisdn)s,now(),now(),%(in_text)s,\'Line\'::text)"
        logging.info ("SQL to observer=%s" % sql)
        sth = self.dbh.cursor()
        query = sql
        logging.debug (query)
        logging.debug ("in_msisdn: %s, in_text: %s" % (in_msisdn, in_text))
        sth.execute (query, { "in_msisdn": in_msisdn, "in_text": in_text })

        if (sth.rowcount <= 0):
            sth.close() 
            logging.error ("zero data rows returned")

            return None
        
        result = sth.fetchone()[0]
        sth.close() 

        logging.debug ("updated messages up to id %s" % (result,))

        return result

    def update_message (self, sm_id, in_text):
        sql = "update cc.\"cc$auto_in_socialmedia\" set payload=%(in_text)s where \"cc$auto_in_socialmedia_id\"=%(sm_id)s"
        logging.info ("SQL to observer=%s" % sql)
        sth = self.dbh.cursor()
        query = sql
        logging.debug (query)
        logging.debug ("sm_id: %s, in_text: %s" % (sm_id, in_text))
        sth.execute (query, { "sm_id": sm_id, "in_text": in_text })

        if (sth.rowcount <= 0):
            sth.close() 
            logging.error ("update failed")

            return False
        sth.close() 

        logging.debug ("updated messages up to id %s" % (sm_id,))

        return True

    def add_to_attachments (self, key_type, key_id, filename, obj_store_rel_path):
        sql = "insert into ct.attachment (key_type, key_id, filename, create_ts, object_store_relative_path) values (%(key_type)s, %(key_id)s, %(filename)s, now(), %(obj_store_rel_path)s)"
        sth = self.dbh.cursor()
        query = sql
        logging.debug (query)
        logging.debug ("key_type: %s\nkey_id: %s\nfilename: %s\npath: %s" % (key_type, key_id, filename, obj_store_rel_path))
        sth.execute (query, { "key_type": key_type, "key_id": key_id, "filename": filename, "obj_store_rel_path": obj_store_rel_path })

        if (sth.rowcount <= 0):
            sth.close() 
            logging.error ("insertion into ct.attachment failed")

            return False
        sth.close() 

        logging.debug ("inserted into ct.attachment")

        return True

    def connect_database (self):         
        try:
            logging.info ('connecting to db with dsn =%s', self.dsn)
            dbh = psycopg2.connect (self.dsn)
            dbh.set_isolation_level (psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
            dbh.set_client_encoding ('UTF8')
            logging.info ('connected to db')
            self.dbh = dbh

            return True

        except Exception as ex:
            logging.exception ("failed to connect to database: " + str (ex))
 
            return False
