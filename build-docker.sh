#!/bin/bash

TAG=$1

if [ -z "$TAG" ]
  then
    echo "Tag for docker image required"
    exit 1
fi

echo removing contents from the dist folder
rm ./dist/*

echo building app
python setup.py bdist_wheel

echo building docker image
sudo docker build -t as-docker-registry.cartrack.com/internal/comms-engine:$TAG .

